package normalizadorFNC;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class ProduccionSinNulleables {

	List<Integer> columnasConNulleables = new ArrayList<>();
	String entrada;
	List<Character> variablesNulleables = new ArrayList<>();
	List<Character> columnaValores = new ArrayList<>();
	Character[][] matrizRespuesta;
	
	public ProduccionSinNulleables(String ent, List<Character> variablesNulleables2) {
		this.entrada = ent;
		this.variablesNulleables = variablesNulleables2;
		limpiarProduccion();
	}
	
	private void limpiarProduccion() {
		int cantFilas = getCantFilas(this.entrada);    	
    	matrizRespuesta = crearMatriz(getCantRespuesta(columnasConNulleables), cantFilas);
		cargarValoresAmatriz();
	}
	
	// Utilizado para generar las posibles combinaciones de las variables nulleables cuando est�n o no est�n
	private void cargarValoresAmatriz() {
		cargarEntradaOriginal();
		if(matrizRespuesta.length < 2) { 
			return ;
		}	
		int cantToMatain = 1;
		int exponente = 1;
		boolean cambio = false;
		for(int i = columnasConNulleables.size()-1; i > -1; i--) {
			for(int e = 0; e < matrizRespuesta.length; e++) {
				if(cambio) {
					matrizRespuesta[e][columnasConNulleables.get(i)] = '-';
				}
				if((e+1) % cantToMatain == 0) {
					cambio = !cambio;
				}
			}
			cantToMatain = (int) Math.pow(2, exponente);
			exponente++;
			cambio = false;
		}
	}

	// COmpleta la matriz con los caracteres del string de input
	private void cargarEntradaOriginal() {
		if(matrizRespuesta.length > 0 && matrizRespuesta[0].length == entrada.length()) {
			for(int e = 0; e < matrizRespuesta.length; e++) {
				for(int i = 0; i < entrada.length(); i++) {
					matrizRespuesta[e][i] = entrada.charAt(i);
				}
			}
		}
	}

	// Encargado de inicializar la matriz
	private Character[][] crearMatriz(int cantResp, int cantFilas) {
		return new Character[cantResp][cantFilas];
	}

	// Obtiene el numero de filas que se necesitar�n para la matriz
	private int getCantFilas(String text) {
		for(int i = 0; i < text.length(); i++) {
			if(variablesNulleables.contains(text.charAt(i))) {
				columnasConNulleables.add(i);
			}
			columnaValores.add(text.charAt(i));
		}
		return columnaValores.size();
	}
	
	// Obtiene la canidad de combinaciones posibles para hacer
	private int getCantRespuesta(List<Integer> variables) {
		return (int) Math.pow(2, variables.size());
	}
	
	// devuelve un arraylist con todas las combinaciones de los lados derechos cuando una variable es nulleable
	public ArrayList<String> limpiarGramatica() { 
		HashSet<String> producciones = new HashSet<String>();		
		String s = "";
		for (int i = 0; i < matrizRespuesta.length; i++) {
		    for (int j = 0; j < matrizRespuesta[i].length; j++) {
		       	if(matrizRespuesta[i][j].equals('-')) {
		        	s += "";
		        }
		        else {
		        	s += matrizRespuesta[i][j];
		        }
		    }
		    if(!s.equals("") && !s.equals("�")) {
		    	producciones.add(s);
		    }	
		    s = "";
		}
		return new ArrayList<String>(producciones);
    }	 	
}