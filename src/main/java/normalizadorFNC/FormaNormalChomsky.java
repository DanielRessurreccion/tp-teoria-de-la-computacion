package normalizadorFNC;

import java.util.ArrayList;
import java.util.HashSet;
import org.apache.commons.math3.util.Pair;
import Auxiliares.Auxiliares;

public class FormaNormalChomsky {

	private ArrayList<Pair<String,String>> gramatica = new ArrayList<Pair<String,String>>();
	private ArrayList<Pair<String,String>> resultadoFNC = new ArrayList<Pair<String,String>>();
	private ArrayList<Character> letrasVariables = new ArrayList<>();
	
	public FormaNormalChomsky(ArrayList<Pair<String,String>> gramatica, ArrayList<Character> variables) {
		this.gramatica = gramatica;
		this.letrasVariables = variables;
		iniciarNormalización();
	}

	private void iniciarNormalización() {
		ArrayList<Pair<String,String>> pasoUno = renombrarTerminalesConVariables();
		reemplazarTerminalesDerechosPorVariables(pasoUno);
		reacomodarProduccionesEnVariables(pasoUno);
	}

	// busca todos los símbolos terminales en los lados derechos y agrega a la 
	// gramática una variable con una producción que deriva en dichos símbolos
	private ArrayList<Pair<String,String>> renombrarTerminalesConVariables() {
		ArrayList<Pair<String,String>> intermedioUno = new ArrayList<Pair<String,String>>();
		
		for(Pair<String,String> par : this.gramatica) {
			String s = par.getValue();				
			for(int i = 0; i < s.length(); i++) {			
				if(	!this.letrasVariables.contains(s.charAt(i)) ) {
					String ch = Character.toString(s.charAt(i));
					if( !estaContenida(this.gramatica, ch) && !estaContenida(intermedioUno, ch) ) {	
						intermedioUno.add(new Pair<String,String>( getNextKey(intermedioUno), ch  ));
					}	
				}	
			}
		}
		this.gramatica.addAll(intermedioUno);
		
		for(Pair<String,String> tupla2 : this.gramatica) {
			if(tupla2.getValue().length() == 1 && !this.letrasVariables.contains(tupla2.getValue().charAt(0)) && !intermedioUno.contains(tupla2)) {
				intermedioUno.add(tupla2);
			}
		}
		return intermedioUno;
	}

	// Busca los terminales de los lados derechos y a cada uno, lo reemplaza por una variables que derive en los mismos
	private void reemplazarTerminalesDerechosPorVariables(ArrayList<Pair<String,String>> intermedioDos) {
		boolean contieneTerminales = false;
		ArrayList<Pair<String,String>> producciones = new ArrayList<Pair<String,String>>();	
		
		for(Pair<String,String> tupla : this.gramatica) {
			String s = tupla.getValue();
			for(Pair<String,String> par : intermedioDos) {	
				if(s.length() > 1 && s.contains(par.getValue())) {	
					String nuevaProd = reemplazarCaracteres(s, intermedioDos);				
					Pair<String,String> nuevaTupla = new Pair<String,String>(tupla.getKey(),nuevaProd);				
					if(!producciones.contains(nuevaTupla)) {
						producciones.add( nuevaTupla );
					}					
				}
				else { 
					if(  s.length() == 1 &&  !producciones.contains(tupla)) {
						producciones.add(tupla);
					}				
				}
			}
			if(s.length() > 1) {
				for(Pair<String,String> parConTerminales : intermedioDos) {	
					if(s.contains( parConTerminales.getValue() ) ) {
						contieneTerminales = true;
					}
				}
				if(!contieneTerminales && !producciones.contains(tupla)) {
					producciones.add(tupla);
				}
				contieneTerminales = false;
			}	
		}	
		this.gramatica = (ArrayList<Pair<String,String>>) producciones.clone(); 		
	}
	
	// Transforma los lados derechos de las producciones para que queden con exactamente 1 terminal o 2 variables
	private void reacomodarProduccionesEnVariables(ArrayList<Pair<String,String>> listaDeClavesNuevas ) {
		ArrayList<Pair<String,String>> resultadoFinal = new ArrayList<Pair<String,String>>();
		for(Pair<String,String> tupla : this.gramatica) {			
			if( Auxiliares.largoDeLaProduccion(tupla.getValue()) <= 2 ) {
				resultadoFinal.add(tupla);
			}
			else {				
				ArrayList<String> variables = Auxiliares.separarProduccionesVariables(tupla.getValue(), this.letrasVariables);
				resultadoFinal.addAll( dividirProducciones( tupla.getKey(), variables, listaDeClavesNuevas) );
			}
		}
		this.resultadoFNC = (ArrayList<Pair<String,String>>) resultadoFinal;
	}
	
	// Utilizado para obtener la siguiente letra que se usará como variable a
	// la hora de renombrar partes de los lados derechos de las producciones
	private String getNextKey( ArrayList<Pair<String,String>> tuplas) {
		HashSet<String> claves = new HashSet<String>();
		for(Pair<String,String> par : tuplas) {	
			claves.add(par.getKey());
		}	
		int cont = claves.size()/this.letrasVariables.size();
		
		for(Character c : this.letrasVariables) {
			String str = Character.toString(c) + "{" + cont + "}";
			if(!claves.contains(str)) {
				return str;
			}
		}		
		return null;
	}
	
	// verifica si el valor pasado como parámetro se encuentra en alguna de las tuplas del arraylist
	private boolean estaContenida(ArrayList<Pair<String,String>> tuplas, String valor) {
		for(Pair<String,String> par : tuplas) {
			if(par.getValue().equals(valor)) {
				return true;
			}
		}
		return false;
	}
	
	// Busca en las tuplas una variable que derive en los símbolos terminales que contenga
	// el string pasado por parámetro y reemplaza dichos símbolos por la variable encontrada 
	private String reemplazarCaracteres(String s, ArrayList<Pair<String, String>> intermedioUno) {
		ArrayList<String> resParcial = new ArrayList<String>();
		for(int i = 0; i < s.length(); i++) {
			resParcial.add(Character.toString(s.charAt(i)));
		}
		for(Pair<String, String> par : intermedioUno) {
			if(resParcial.contains(par.getValue())) {
				for(int k = 0; k < resParcial.size(); k++) {
					if(resParcial.get(k).equals(par.getValue()) ) {		
						resParcial.set(k, par.getKey());
					}
				}
			}
		}
		String str = "";
		for(String caracter: resParcial) {
			str += caracter;
		}		
		return str;
	}
	
	// Divide las producciones de mas de 2 variables, creando nuevas producciones
	// hasta que todos los lados derechos queden con exactamente 2 variables
	private ArrayList<Pair<String,String>> dividirProducciones(String clave, ArrayList<String> variables, ArrayList<Pair<String,String>> clavesNuevas){	
		ArrayList<Pair<String,String>> producciones = new ArrayList<Pair<String,String>>();
		int largoInicial = variables.size();
		String keyGeneradaAnterior = "";
		
		while(variables.size() > 2) {
			String proxClave = "";
			String prodNueva = "";	
			String keyGenerada = getNextKey(clavesNuevas);			
			if(variables.size() == largoInicial) {
				proxClave = clave;
			}
			else {
				proxClave = keyGeneradaAnterior;
			}
			prodNueva = variables.get(0) + keyGenerada;
			Pair<String,String> tup = new Pair<String,String>(proxClave, prodNueva); 
			producciones.add(tup);
			clavesNuevas.add(tup);
			variables.remove(0);
			keyGeneradaAnterior = keyGenerada;
		}	
			
		if(variables.size() == 2) {
			String ultimaClave = getNextKey(clavesNuevas);
			String ultimaProd = variables.get(0) + variables.get(1);
			Pair<String,String> par = new Pair<String,String>(ultimaClave, ultimaProd); 
			producciones.add(par);
			clavesNuevas.add(par);
		}
		
		return producciones;
	}
	
	public ArrayList<Pair<String, String>> getResultadoFNC() {
		return resultadoFNC;
	}
}