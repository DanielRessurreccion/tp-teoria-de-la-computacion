package normalizadorFNC;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class ProduccionesSoloConAlcanzables {

	private HashMap<String,ArrayList<String>> gramatica;
	private HashMap<String,ArrayList<String>> resultado;
	private ArrayList<String> variables;
	private ArrayList<String> simbolosAlcanzables;
	
	public ProduccionesSoloConAlcanzables(HashMap<String,ArrayList<String>> grr) {
		this.gramatica = grr;
		this.variables = new ArrayList<String>();
		this.simbolosAlcanzables = new ArrayList<String>();
		limpiarNoAlcanzables();
	}

	private void limpiarNoAlcanzables() {
		getSimbolosAlcanzables();
		quitarSimbolosNoAlcanzables();
	}

	// Utilizado para descubrir cuales son las variables alcanzables desde el s�mbolo inicial "S"
	private void getSimbolosAlcanzables() {
		for (Entry<String, ArrayList<String>> entry : this.gramatica.entrySet()) {
		    String key = entry.getKey();
		    this.variables.add(key);
		}
		
		simbolosAlcanzables.add("S");
		int cont = 0;
		while(cont < simbolosAlcanzables.size()) {			
			if(this.gramatica.containsKey(simbolosAlcanzables.get(cont))) {
				ArrayList<String> prods = this.gramatica.get(simbolosAlcanzables.get(cont));
				for(String s : prods) {
					for(int i = 0; i < s.length(); i++) {
						String str = Character.toString(s.charAt(i));
						if(this.variables.contains(str) && !simbolosAlcanzables.contains(str)) {
							simbolosAlcanzables.add(str);
						}
					}	
				}
				cont ++;
			}
		}
	}
	
	// Encargado de eliminar de la gram�tica los s�mbolos que no sean alcanzables desde "S"
	private void quitarSimbolosNoAlcanzables() {	
		this.resultado = (HashMap<String, ArrayList<String>>) this.gramatica.clone();
		ArrayList<String> simbolosNoAlcanzables = new ArrayList<String>();
		
		for(String s : this.variables) {
			if(!simbolosAlcanzables.contains(s)) {
				simbolosNoAlcanzables.add(s);
				this.resultado.remove(s);
			}
		}
	}
	
	public HashMap<String, ArrayList<String>> getResultado() {
		return this.resultado;
	}
}
