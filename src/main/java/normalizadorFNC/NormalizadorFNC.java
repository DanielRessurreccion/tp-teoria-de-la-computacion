package normalizadorFNC;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import org.apache.commons.math3.util.Pair;
import Auxiliares.Auxiliares;

public class NormalizadorFNC {

	private HashMap<String,ArrayList<String>> gramatica = new HashMap<String,ArrayList<String>>();
	
	private HashMap<String,ArrayList<String>> resultadoSinNulleables = new HashMap<String,ArrayList<String>>();
	private HashMap<String,ArrayList<String>> resultadoSinUnitarias = new HashMap<String,ArrayList<String>>();
	private HashMap<String,ArrayList<String>> resultadoSoloGeneradores = new HashMap<String,ArrayList<String>>();
	private HashMap<String,ArrayList<String>> resultadoSoloAlcanzables = new HashMap<String,ArrayList<String>>();
	private ArrayList<Pair<String,String>> tuplasFNC = new ArrayList<Pair<String,String>>();	
	private List<Character> variablesNulleables = new ArrayList<>();
	private ArrayList<Character> letrasVariables = new ArrayList<>();

	public NormalizadorFNC(HashMap<String,ArrayList<String>> gr) {
		this.gramatica = gr;
		validarClavesVac�as();	
		this.letrasVariables = Auxiliares.completarPosiblesVariables();
		limpiarProduccionesEpsilon();			
		limpiarProduccionesUnitarias();	
		limpiarSimbolosNoGeneradores();
		limpiarSimbolosNoAlcanzables();
 		llevarAgramaticaFNC();
	}
		
	// Por cada arraylist de producciones de la gram�tica inicial, devuelve otro arraylist con  
	// dichas producciones luego de pasar por el algoritmo de eliminar producciones epsilon
	private void limpiarProduccionesEpsilon() {
		this.variablesNulleables = getVariablesNulleables();
		for (Entry<String, ArrayList<String>> entry : this.gramatica.entrySet()) {
		    String key = entry.getKey();
		    ArrayList<String> produccionesNuevas = new ArrayList<String>();
		    for(String s : entry.getValue()) {
		    	ProduccionSinNulleables psn = new ProduccionSinNulleables(s, this.variablesNulleables);
		    	produccionesNuevas.addAll(psn.limpiarGramatica());		
		    }
		    this.resultadoSinNulleables.put(key, produccionesNuevas);
		}
	}
	
	// Para la gram�tica sin producciones epsilon, devuelve HashMap<String, ArrayList<String>>  
	// con dicha gram�tica luego de pasar por el algoritmo de eliminar producciones unitarias
	private void limpiarProduccionesUnitarias() {
		ProduccionesSinUnitarias psu = new ProduccionesSinUnitarias(this.resultadoSinNulleables, this.letrasVariables);
		this.resultadoSinUnitarias = psu.getResultado();
	}	
	
	// Para la gram�tica sin producciones unitarias, devuelve HashMap<String, ArrayList<String>>  
	// con dicha gram�tica luego de pasar por el algoritmo de eliminar simbolos no generadores
	private void limpiarSimbolosNoGeneradores() {
		ProduccionesSoloConGeneradores psg = new ProduccionesSoloConGeneradores(getSimbolosGeneradores(), this.resultadoSinUnitarias, this.letrasVariables);	
		this.resultadoSoloGeneradores = psg.getResultado();
	}

	// Para la gram�tica sin s�mbolos no generadores, devuelve HashMap<String, ArrayList<String>>  
	// con dicha gram�tica luego de pasar por el algoritmo de eliminar simbolos no alcanzables
	private void limpiarSimbolosNoAlcanzables(){
		ProduccionesSoloConAlcanzables psca = new ProduccionesSoloConAlcanzables(this.resultadoSoloGeneradores);
		this.resultadoSoloAlcanzables = psca.getResultado();
	}
	
	// Para la gram�tica sin simbolos no alcanzables, devuelve ArrayList<Pair<String,String>> con dicha 
	// gram�tica luego de pasar por el algoritmo para transformar la gram{atica a la Forma Normal de Chomsky
	private void llevarAgramaticaFNC() {
		FormaNormalChomsky gramaticaFNC = new FormaNormalChomsky( pasarAtuplas(this.resultadoSoloAlcanzables) , this.letrasVariables);
		this.tuplasFNC = gramaticaFNC.getResultadoFNC();
	}
	
	// De cada produccion pasada como par�metro (String s), devuelve "true" 
	// si todos sus caracteres/s{imbolos est�n en el conjunto de nulleables
	// Nota: consideramos el caracter "�" como epsilon
	private boolean esNulleable(String s, HashSet<Character> nulleables) {
		if(s.equals("�")) {
    		return true;
    	}
		else {
			int cont = 0;
			for(int i = 0; i < s.length(); i++) {
				Character c = s.charAt(i);
				if(nulleables.contains(c)) {
    				cont ++;
    			}
    		}
			if(cont == s.length()) {
				return true;
			}
		}
		return false;
	}

	// De la gram{atica inicial, obtiene las variables que sean nulleables
	private ArrayList<Character> getVariablesNulleables() {	
		HashSet<Character> nulleables = new HashSet<Character>();
		HashSet<Character> iteracion = new HashSet<Character>();
		do {	
			nulleables = (HashSet<Character>) iteracion.clone();	
			for (Entry<String, ArrayList<String>> entry : this.gramatica.entrySet()) {
			    String key = entry.getKey();
			    for(String s : entry.getValue()) {
			    	if(esNulleable(s, nulleables)) {		    		
		    		   iteracion.add(key.charAt(0));
			    	}
			    }
			}
		}
		while(!nulleables.equals(iteracion));
		return new ArrayList<Character>(nulleables);
	}
	
	// De la gram�tica sin producciones unitarias, obtiene los simbolos generadores
	private ArrayList<String> getSimbolosGeneradores() {	
		ArrayList<Character> variables = this.letrasVariables;
		ArrayList<Character> letrasTerminales = pasarAlowerCase( (variables) );
		HashSet<String> generadores = new HashSet<String>();
		HashSet<String> iteracion = new HashSet<String>();
		do {	
			generadores = (HashSet<String>) iteracion.clone();

			for (Entry<String, ArrayList<String>> entry : this.resultadoSinUnitarias.entrySet()) {			
				String key = entry.getKey();
			    for(String s : entry.getValue()) {
		    		int contLargo = 0;
		    		String res = "";
		    		
		    		for(int i = 0; i < s.length(); i++) {
		    			if(generadores.contains( String.valueOf(s.charAt(i)) ) ) {
		    				contLargo ++;
		    			}	
		    			if(!variables.contains(s.charAt(i)) && !letrasTerminales.contains(s.charAt(i))  ) {
		    				String term = Character.toString(s.charAt(i));
		    				iteracion.add(term);
		    			}
		    			if( letrasTerminales.contains(s.charAt(i)) ) {
		    				res += s.charAt(i);
		    				if(s.length() == (i+1) || !letrasTerminales.contains(s.charAt(i+1)) ) {
		    					iteracion.add(res);
		    					res = "";
		    				}	
		    			}
		    		}
		    		if(contLargo == s.length() ) {
		    			iteracion.add(key);
		    		}
		    		if(s.length() == 1 && !variables.contains(s.charAt(0))) {
			    		iteracion.add(s);
			    	}
			    	if(generadores.contains(s)) {
			    		iteracion.add(key);
			    	}
			    }
			}		
		}
		while(!generadores.equals(iteracion));
		
		return new ArrayList<String>(generadores);
	}	
	
	// utilizado para pasar a min�scula la lista de Variables 
	private ArrayList<Character> pasarAlowerCase(ArrayList<Character> arr){
		ArrayList<Character> resultado = new ArrayList<Character>();
		for(Character str : arr) {
			String res = str.toString().toLowerCase();
			resultado.add(res.charAt(0));
		}
		return resultado;
	}
	
	// utilizado para convertir el HashMap<String,ArrayList<String>> con la gram�tica de s�mbolos alcanzables
	// en un ArrayList<Pair<String,String>> que se le enviar� como par�metro de la instancia de FormaNormalChomsky
	private ArrayList<Pair<String,String>> pasarAtuplas(HashMap<String,ArrayList<String>> gram){
	    ArrayList<Pair<String,String>> tuplasArray = new ArrayList<Pair<String,String>>();
		for (Entry<String, ArrayList<String>> entry : gram.entrySet()) {
		    String key = entry.getKey();
		    for(String s : entry.getValue()) {
		    	Pair<String,String> tupla = new Pair<String,String>(key,s);
		    	tuplasArray.add(tupla);
		    }
		}
		return tuplasArray;
	}
	
	// De la gram�tica inicial, se encarga de buscar produciones vac�as (espacios vac�os en el archivo) y eliminarlas, en caso de encontrarlas
	private void validarClavesVac�as() {
		if(this.gramatica.containsKey("")) {
			this.gramatica.remove("");
		}
	}
	
	public HashMap<String, ArrayList<String>> getResultadoSinNulleables() {
		return resultadoSinNulleables;
	}
	
	public HashMap<String, ArrayList<String>> getResultadoSinUnitarias() {
		return resultadoSinUnitarias;
	}

	public HashMap<String, ArrayList<String>> getResultadoSoloGeneradores() {
		return resultadoSoloGeneradores;
	}

	public HashMap<String, ArrayList<String>> getResultadoSoloAlcanzables() {
		return resultadoSoloAlcanzables;
	}

	public ArrayList<Pair<String, String>> getTuplasFNC() {
		return tuplasFNC;
	}
}