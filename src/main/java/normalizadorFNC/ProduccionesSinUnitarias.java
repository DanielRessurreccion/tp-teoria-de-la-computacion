package normalizadorFNC;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import org.apache.commons.math3.util.Pair;

public class ProduccionesSinUnitarias {

	private HashMap<String,ArrayList<String>> gramatica;
	private HashMap<String,ArrayList<String>> resultado;
	private ArrayList<Character> letrasVariables = new ArrayList<>();
	private ArrayList<Pair<String,String>> tuplas;
	
	public ProduccionesSinUnitarias(HashMap<String,ArrayList<String>> gr, ArrayList<Character> vars) {
		this.gramatica = gr;
		this.letrasVariables = vars;
		this.tuplas = new ArrayList<Pair<String,String>>();
		getProduccionesIniciales();
		rearmarProducciones();
	}

	// Obtiene todas las tuplas de producciones y luego busca los lados derechos de las tuplas que sean lados izquierdos
	// en otra tupla y agrega una nueva tupla con el lado izquierdo de la primera y el lado derecho de la segunda 
	private void getProduccionesIniciales() {
		ArrayList<Pair<String,String>> iteracion =  new ArrayList<Pair<String,String>>();
		for (Entry<String, ArrayList<String>> entrada : this.gramatica.entrySet()) {
		    String key = entrada.getKey();
		    Pair<String,String> t = new Pair<String,String>(key,key);
		    if(!iteracion.contains(t)) {
    			iteracion.add(t);
    		}	    
		    for(String s : entrada.getValue()) {
		    	if(s.length() == 1 && this.letrasVariables.contains(s.charAt(0))) {
		    		Pair<String,String> tup = new Pair<String,String>(s,s);
		    		if(!iteracion.contains(tup)) {
		    			iteracion.add(tup);
		    		}
		    	}
		    }		    
		}  
			
		do {	
			this.tuplas = (ArrayList<Pair<String,String>>) iteracion.clone();		
			for (Entry<String, ArrayList<String>> entry : this.gramatica.entrySet()) {
			    String key = entry.getKey();
			    for(String s : entry.getValue()) {
			    	if(this.gramatica.containsKey(s) && !key.equals(s)) {
			    		Pair<String,String> par = new Pair<String,String>(key,s);
			    		if(!iteracion.contains(par)) {
			    			iteracion.add(par);
			    		}
			    	}			    	
			    	if(!key.equals(s) && s.length() == 1 && this.letrasVariables.contains(s.charAt(0))) {
			    		Pair<String,String> tup = new Pair<String,String>(key,s);
			    		if(!iteracion.contains(tup)) {
			    			iteracion.add(tup);
			    		}
			    	}
			    }
			}
		}
		while(!this.tuplas.equals(iteracion));
		
		do {	
			this.tuplas = (ArrayList<Pair<String,String>>) iteracion.clone();		
			for(Pair<String,String> tupla: this.tuplas) {
				for(Pair<String,String> tupla2: this.tuplas) {
					if(tupla.getValue().equals(tupla2.getKey())) {
						Pair<String,String> nuevo = new Pair<String,String>(tupla.getKey(), tupla2.getValue());
						if(!iteracion.contains(nuevo)) {	
							iteracion.add(nuevo);
						}	
					}
				} 
			}
		}
		while(!this.tuplas.equals(iteracion));
	}
	
	// Recorre las tuplas y rearma la gram�tica, con solo las producciones que derivan a alg�n terminal en 0 o m�s pasos
	private void rearmarProducciones() {	
		HashMap<String,ArrayList<String>> iteracion = new  HashMap<String,ArrayList<String>>();
		
		for (Entry<String, ArrayList<String>> entry : this.gramatica.entrySet()) {
		    String key = entry.getKey();
		    ArrayList<String> prods = new ArrayList<String>();
		    for(String s : entry.getValue()) {
		    	if(!this.gramatica.containsKey(s)) {
		    		prods.add(s);
		    	}
		    }
		    iteracion.put(key, prods);
		}
		
		do {	
			this.resultado = (HashMap<String,ArrayList<String>>) iteracion.clone();		
			for(Pair<String,String> tupla: this.tuplas) {
				for(Pair<String,String> tupla2: this.tuplas) {
					if(tupla.getValue().equals(tupla2.getKey()) && !tupla.getKey().equals(tupla2.getValue()) ) {			
						ArrayList<String> produccionesKey = new ArrayList<String>();	
						if( this.gramatica.containsKey(tupla2.getKey()) ) {
							produccionesKey = this.resultado.get(tupla2.getKey());
							if(produccionesKey != null) {
								for(String str : produccionesKey) {
									if(  !iteracion.get(tupla.getKey()).contains(str) ) {
										iteracion.get(tupla.getKey()).add(str);
									}
								}
							}
						}			
					}
				} 
			}
		}
		while(!this.resultado.equals(iteracion));
	
		this.resultado = iteracion;
	}
	
	public HashMap<String, ArrayList<String>> getResultado() {
		return resultado;
	}
}