package normalizadorFNC;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class ProduccionesSoloConGeneradores {

	private HashMap<String,ArrayList<String>> gramatica;
	private HashMap<String,ArrayList<String>> resultado;
	private ArrayList<String> generadores;
	private ArrayList<Character> letrasVariables;
	private ArrayList<Character> noGeneradores = new ArrayList<Character>();
	
	public ProduccionesSoloConGeneradores(ArrayList<String> arr, HashMap<String,ArrayList<String>> gr, ArrayList<Character> vars) {
		this.generadores = arr;
		this.gramatica = gr;
		this.letrasVariables = vars;
		getSimbolosNoGeneradores();
		limpiarNoGeneradores();
	}

	// Se encarga de eliminar de la gramática los símbolos no generadores y las producciones en las que aparece
	private void limpiarNoGeneradores() {
		this.resultado = (HashMap<String, ArrayList<String>>) this.gramatica.clone();		
		for (Entry<String, ArrayList<String>> entrada : this.gramatica.entrySet()) {
		    String key = entrada.getKey();
		    if(this.noGeneradores.contains(key.charAt(0))) {
		    	this.resultado.remove(key);
		    }
		    else {
		    	ArrayList<String> copiaProds = (ArrayList<String>) entrada.getValue().clone();
			    for(String s : copiaProds) {
				    for(int i = 0; i < s.length(); i++) {	
			    		if(this.noGeneradores.contains(s.charAt(i))) {				    		
			    			this.resultado.get(key).remove(s);
				    		break;
				    	}
				    }	
			    } 
			}
		}	
	}

	// De la gramática sin producciones unitarias, obtiene los símbolos no generadores
	private void getSimbolosNoGeneradores() {
		this.noGeneradores = new ArrayList<Character>();
		for (Entry<String, ArrayList<String>> entry : this.gramatica.entrySet()) {
		    String key = entry.getKey();   
		    if(!this.generadores.contains(key) && !this.noGeneradores.contains(key.charAt(0))) {   	
		    	this.noGeneradores.add(key.charAt(0));
		    }
		    for(String st : entry.getValue()) {
			    for(int i = 0; i < st.length(); i++) {
			    	String nuevo = Character.toString(st.charAt(i));
		    		if(!this.generadores.contains(nuevo) && this.letrasVariables.contains(st.charAt(i)) ) {				    		
		    			this.noGeneradores.add(st.charAt(i));
			    	}
			    }	
		    }
		} 
	}
		
	public HashMap<String, ArrayList<String>> getResultado() {
		return this.resultado;
	}	
}
