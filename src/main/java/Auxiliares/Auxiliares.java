package Auxiliares;

import java.util.ArrayList;

public class Auxiliares {

	public static int largoDeLaProduccion(String str) {
		int cont = 0;
		boolean llaveAbierta = false;
		for(int i = 0; i < str.length(); i++) {
			if(str.charAt(i) == '{') {
				llaveAbierta = true;
				continue;
			}
			else if(str.charAt(i) == '}') {
				llaveAbierta = false;
				continue;
			}
			else if(llaveAbierta) {
				continue;
			}
			else {
				cont ++;
			}
		}		
		return cont;
	}
	
	public static ArrayList<String> separarProduccionesVariables(String prod, ArrayList<Character> letrasVariables){
		ArrayList<String> resultado = new ArrayList<String>();
		boolean llaveAbierta = false;
		String acumulador = "";
		
		for(int i = 0; i < prod.length(); i++) {
			String ch = Character.toString(prod.charAt(i));
			if(letrasVariables.contains(prod.charAt(i))) {
				acumulador += ch;
				if( (i + 1) < prod.length() && letrasVariables.contains( prod.charAt(i+1) ) ) {
					resultado.add(acumulador);
					acumulador = "";
				}
			}
			else if( ch.equals("{") ) {
				llaveAbierta = true;
				acumulador += ch;
			}
			else if( ch.equals("}") ) {
				llaveAbierta = false;
				acumulador += ch;
				resultado.add(acumulador);
				acumulador = "";
			}
			else if( llaveAbierta ) {
				acumulador += ch;
			}
			if(i == ( prod.length() - 1 ) && !acumulador.equals("") ) {
				resultado.add(acumulador);
				acumulador = "";
			}
		}		
		return resultado;
	}
	
	public static ArrayList<Character> completarPosiblesVariables(){
		ArrayList<Character> letrasVariables = new ArrayList<Character>();
		Character[] letras = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 
				   'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
		for(int i = 0; i < letras.length; i++) {
			letrasVariables.add(letras[i]);
		}
		return letrasVariables;
	}	
	
}
