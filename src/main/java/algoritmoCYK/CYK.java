package algoritmoCYK;

import java.util.ArrayList;
import org.apache.commons.math3.util.Pair;
import Auxiliares.Auxiliares;

public class CYK {

	private ArrayList<Pair<String,String>> gramatica = new ArrayList<Pair<String,String>>();	
	private ArrayList<Character> letrasVariables = new ArrayList<>();
	private String inputStr;
	private String simboloInicial;
	
	public CYK(ArrayList<Pair<String,String>> gram, String paraParsear) {
		this.gramatica = gram;
		this.inputStr = paraParsear;
		this.simboloInicial = "S";
		this.letrasVariables = Auxiliares.completarPosiblesVariables();
	}

	// Primero completa la fila inicial para cada caracter del input
	// Luego obtiene los conjuntos posibles y completa un ArrayList de tuplas
	// reconoce el string, si el s�mbolo inicial se encuentra en el conjunto del indice (1, largo del string)
	public boolean parserCyk() {
		int largo = this.inputStr.length();	
	    ArrayList<Pair<String, ArrayList<String>>> resultado = new 	    ArrayList<Pair<String, ArrayList<String>>>();
			
		for(int i = 0; i < largo; i++) {
			ArrayList<String> iteracion = new ArrayList<String>();
			String s = Character.toString(inputStr.charAt(i) );
			
			for(Pair<String,String> par : this.gramatica) {
				if(par.getValue().contentEquals(s)) {
					iteracion.add(par.getKey());
				}
			}
			String clave = "" + (i+1) + (i+1);
			Pair<String, ArrayList<String>> parNuevo = new Pair<String, ArrayList<String>>(clave,iteracion); 
			resultado.add(parNuevo);
		}
		
		int cantConjuntos = 1;

		while(cantConjuntos < largo) {
			for(int i = 1; (i + cantConjuntos) <= largo; i++) {				
				ArrayList<String> resParcial = new ArrayList<String>();
				
				for(int k = i; k <= (i + cantConjuntos); k++) {				
					ArrayList<String> primVar = getConjunto( i, k, resultado); 								
					ArrayList<String> secuVar = getConjunto( (k + 1), ( cantConjuntos + i), resultado); 	
					
					for(String prod1 : primVar) {
						for(String prod2 : secuVar) {						
							for(Pair<String,String> par : this.gramatica) {								
								if(Auxiliares.largoDeLaProduccion(par.getValue()) == 2) {		
									ArrayList<String> prodSeparada = Auxiliares.separarProduccionesVariables(par.getValue(), this.letrasVariables);
									if(prodSeparada.get(0).equals(prod1) && prodSeparada.get(1).equals(prod2)) {	
										if(!resParcial.contains(par.getKey())) {
											resParcial.add(par.getKey());
										}	
									}							
								}
							}
						}
					}
				}	
				String clave = "" + i + (cantConjuntos + i);
				Pair<String, ArrayList<String>> parNuevo1 = new Pair<String, ArrayList<String>>( clave, resParcial ); 
				resultado.add(parNuevo1);
			}
			cantConjuntos ++;
		}
/*		
		System.out.println("------------------------------------------");
		System.out.println("Conjuntos");
		for(Pair<String, ArrayList<String>> p : resultado) {
			System.out.println(p.getKey() + " { " + p.getValue().toString() + " }");
		}
*/
		String claveBuscada = "" + 1 + (largo);
		for(Pair<String, ArrayList<String>> p : resultado) {
			if( p.getKey().equals( claveBuscada ) && p.getValue().contains(simboloInicial)  ) {
				return true;
			}
		}
		return false;
	}
			
	// utilizado para obtener los conjuntos de los indices pasados como par�metro
	private ArrayList<String> getConjunto(int indexIni, int indexFin,  ArrayList<Pair<String, ArrayList<String>>> array){
		ArrayList<String> retorno = new ArrayList<String>();
		String busqueda = "" + (indexIni) + (indexFin);
		for(Pair<String, ArrayList<String>> par: array) {
			if(par.getKey().equals(busqueda)) {
				retorno = par.getValue();
			}
		}
		return retorno;
	}
}