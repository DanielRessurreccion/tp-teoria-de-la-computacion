package automatas;

import java.util.LinkedHashSet;
import java.util.Set;

public class TransicionMultiple {

	Set<Integer> estado1;
	Character input;
	Set<Integer> estado2;
	boolean contieneEstadoFinal;
	
	public TransicionMultiple() {
		this.estado1 = new LinkedHashSet<>();
		this.input =' ';
		this.estado2 = new LinkedHashSet<>();
		this.contieneEstadoFinal = false;
	}
	
	
	
	public boolean isContieneEstadoFinal() {
		return contieneEstadoFinal;
	}

	public void setContieneEstadoFinal(boolean contieneEstadoFinal) {
		this.contieneEstadoFinal = contieneEstadoFinal;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((estado1 == null) ? 0 : estado1.hashCode());
		result = prime * result + ((estado2 == null) ? 0 : estado2.hashCode());
		result = prime * result + ((input == null) ? 0 : input.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return  estado1 + ", " + input + " -> " + estado2;
	}

	public Set<Integer> getEstado1() {
		return estado1;
	}

	public void setEstado1(Set<Integer> estados1) {
		this.estado1.addAll(estados1);
	}

	public Character getInput() {
		return input;
	}

	public void setInput(Character input) {
		this.input = input;
	}

	public Set<Integer> getEstado2() {
		return estado2;
	}

	public void setEstado2(Set<Integer> estados2) {
		this.estado2.addAll(estados2);
	}
	
}
