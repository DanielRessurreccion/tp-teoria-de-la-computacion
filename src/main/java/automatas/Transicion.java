package automatas;

public class Transicion {

	private Integer estado1;
	private Character input;
	private Integer estado2;
	
	public Integer getEstado1() {
		return this.estado1;
	}

	public Character getInput() {
		return input;
	}

	public Integer getEstado2() {
		return estado2;
	}
	
	public Transicion(Integer estado1, Character input, Integer estado2) {
		this.estado1 = estado1;
		this.input = input;
		this.estado2 = estado2;
	}

	@Override
	public String toString() {
		return  estado1 + ", " + input + " -> " + estado2;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((estado1 == null) ? 0 : estado1.hashCode());
		result = prime * result + ((estado2 == null) ? 0 : estado2.hashCode());
		result = prime * result + ((input == null) ? 0 : input.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Transicion other = (Transicion) obj;
		if (estado1 == null) {
			if (other.estado1 != null)
				return false;
		} else if (!estado1.equals(other.estado1))
			return false;
		if (estado2 == null) {
			if (other.estado2 != null)
				return false;
		} else if (!estado2.equals(other.estado2))
			return false;
		if (input == null) {
			if (other.input != null)
				return false;
		} else if (!input.equals(other.input))
			return false;
		return true;
	}
	
	
	
}
