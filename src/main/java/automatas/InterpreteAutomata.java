package automatas;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import lectorDeArchivos.LectorDeArchivos;

public class InterpreteAutomata {

	private String ruta;
	private ArrayList<String> archivo;
	
	private Set<Character> inputs;
	private Integer cantEstados;
	private Set<Integer> estadosFinales;
	private Set<Transicion> transiciones;
	private Map<Integer,Set<Integer>> clausuras;
	
	public InterpreteAutomata(String ruta) {
		this.ruta = ruta;
		this.archivo = LectorDeArchivos.leerArchivo(this.ruta);
		
		this.inputs = extraerInputs(this.archivo.get(0));
		this.cantEstados = Integer.parseInt(this.archivo.get(1));
		this.estadosFinales = extraerEstadosFinales(this.archivo.get(2));
		this.transiciones = extraerTransicioones(this.archivo);
		this.clausuras = new HashMap<>();
		this.obtenerClausuras();
	}
	
	private Set<Transicion> extraerTransicioones(List<String> archivo) {
		int flag=0;
		String estado1, estado2;
		char input = ' ';
		Set<Transicion> t = new LinkedHashSet<>();
		for (int i = 3; i < archivo.size(); i++) {
			validarTransicion(archivo.get(i));
			estado1 ="";
			estado2 ="";
			for (int j = 0; j < archivo.get(i).length(); j++) {
				if(archivo.get(i).charAt(j) >= '0' && archivo.get(i).charAt(j) <= '9') {
					estado1 += archivo.get(i).charAt(j)+"";
					flag = j;
				}
				if(archivo.get(i).charAt(j) == ',') {
					break;
				}
			}
			for (int j = flag+1; j < archivo.get(i).length(); j++) {
				if(Character.isLetter(archivo.get(i).charAt(j)) || archivo.get(i).charAt(j) >= '0' && archivo.get(i).charAt(j) <= '9') {
					input = archivo.get(i).charAt(j);
					flag = j;
					break;
				}
			}
			
			for (int j = flag+1; j < archivo.get(i).length(); j++) {
				if(archivo.get(i).charAt(j) >= '0' && archivo.get(i).charAt(j) <= '9') {
					estado2 += archivo.get(i).charAt(j) +"";
				}
			}
			t.add(new Transicion(Integer.parseInt(estado1),input,Integer.parseInt(estado2)));
		}
		return t;
	}
	
	private void validarTransicion(String s) {
		String transExp = "[0-9]*, [a-zA-Z] -> [0-9]*";
		
		if(!Pattern.matches(transExp, s)){
			throw new RuntimeException("Formato del archivo NO valido, Transicion invalida");
		}
	}

	private Set<Character> extraerInputs(String s) {
		Set<Character> inp = new LinkedHashSet<>();
		for (int i = 0; i < s.length(); i++) {
			if(Character.isLetter(s.charAt(i)) || (s.charAt(i) >= '0' && s.charAt(i) <= '9')){
				inp.add(s.charAt(i));
				
				if(i < s.length()-1) {
					if(Character.isLetter(s.charAt(i+1)) || (s.charAt(i+1) >= '0' && s.charAt(i+1) <= '9')) {
						throw new RuntimeException("Formato del archivo NO valido");
					}
				}
			}
		}
		return inp;
	}
	
	private Set<Integer> extraerEstadosFinales(String s) {
		Set<Integer> inp = new LinkedHashSet<>();
		for (int i = 0; i < s.length(); i++) {
			if(i < s.length()-1) {
				if(Character.isLetter(s.charAt(i+1))) {
					throw new RuntimeException("Formato del archivo NO valido, No puede haber una letra como estado final");
				}
			}
			if(s.charAt(i) >= '0' && s.charAt(i) <= '9'){
				inp.add(Integer.parseInt(s.charAt(i)+""));
			}
		}
		return inp;
	}
	
	private void obtenerClausuras(){
		Set<Integer> clausura;
		for (int i = 1; i <= this.cantEstados; i++) {
			clausura = new LinkedHashSet<Integer>();
			this.obtenerClausura(clausura, i);
			this.clausuras.put(i, clausura);
		}
	}
	
	private void obtenerClausura(Set<Integer> clausuras, Integer i){
		clausuras.add(i);
		for (Transicion t : this.transiciones) {
			if(i == t.getEstado1()) {
				if(t.getInput() == 'E' && !clausuras.contains(t.getEstado2())) {
					this.obtenerClausura(clausuras, t.getEstado2());
				}
			}
		}
	}

	public Set<Character> getInputs() {
		return inputs;
	}

	public Integer getCantEstados() {
		return cantEstados;
	}

	public Set<Integer> getEstadosFinales() {
		return estadosFinales;
	}

	public Set<Transicion> getTransiciones() {
		return transiciones;
	}

	public Map<Integer, Set<Integer>> getClausuras() {
		return clausuras;
	}

	@Override
	public String toString() {
		String s = this.inputs + "\n";
		s += this.cantEstados + "\n";
		s += this.estadosFinales + "\n";
		for (Transicion t : this.transiciones) {
			s += t +"\n";
		}
		return s;
	}
	
	
	
}
