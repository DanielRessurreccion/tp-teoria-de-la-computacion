package automatas;

import java.util.Set;

public class AFD {

	private Set<Character> inputs;
	private Integer cantEstados;
	private Set<Integer> estadosFinales;
	private Set<Transicion> transiciones;
	
	public AFD(TransformarADF automataTransformado) {
		this.inputs =  automataTransformado.getInputs();
		this.cantEstados = automataTransformado.getCantEstados();
		this.estadosFinales = automataTransformado.getEstadosFinales();
		this.transiciones = automataTransformado.getTransiciones();
	}
	
	public boolean procesar(String w) {
		return procesar(w,1);
	}
	
	private boolean procesar(String w, Integer estado) {
		if(!this.inputs.contains(w.charAt(0))){
			return false;
		}
		Integer proximoEstado = this.buscarEstado(w.charAt(0), estado);
		
		if(proximoEstado == null) {
			return false;
		}
		if(w.length() == 1 && !this.esEstadoFinal(proximoEstado)) {
			return false;
		}
		if(w.length() == 1 && this.esEstadoFinal(proximoEstado)) {
			return true;
		}
		else {
			return this.procesar(w.substring(1), proximoEstado);
		}
	}
	
	private Integer buscarEstado(Character c, Integer estado) {
		
		for (Transicion t : transiciones) {
			if(estado == t.getEstado1() && c == t.getInput()) {
				return t.getEstado2();
			}
		}
		return null;
	}
	
	private boolean esEstadoFinal(Integer estado) {
		return this.estadosFinales.contains(estado);
	}

	public Set<Character> getInputs() {
		return inputs;
	}

	public Integer getCantEstados() {
		return cantEstados;
	}

	public Set<Integer> getEstadosFinales() {
		return estadosFinales;
	}

	public Set<Transicion> getTransiciones() {
		return transiciones;
	}
}
