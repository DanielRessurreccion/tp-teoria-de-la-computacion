package automatas;


import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TransformarADF {
	private InterpreteAutomata AFNDe;
	private Set<TransicionMultiple> transicionesMultiples;
	private Set<Set<Integer>> estadosAlmacenados;
	private Map<Set<Integer>,Integer> nuevosEstados;
	
	private Set<Character> inputs;
	private Integer cantEstados;
	private Set<Integer> estadosFinales;
	private Set<Transicion> transiciones;

	public TransformarADF(InterpreteAutomata automata) {
		this.AFNDe = automata;
		this.transicionesMultiples = new LinkedHashSet<>();
		this.obtenerTrancionesMultiples();
		this.agregarEstadosFinales();
		this.nuevosEstados = this.obtenerNuevosEstados();
		
		this.inputs = this.AFNDe.getInputs();
		this.cantEstados = nuevosEstados.size();
		this.estadosFinales = new LinkedHashSet<>();
		this.transiciones = new LinkedHashSet<>();
		this.obtenerNuevasTransiciones();
		this.obtenerNuevosEstadosFinales();
		
	}
	
	private void obtenerTrancionesMultiples() {
		Set<Integer> estados = new LinkedHashSet<>();
		estados.add(1);
		List<Set<Integer>> listaEstados = new LinkedList<>();
		this.estadosAlmacenados = new LinkedHashSet<>();
		listaEstados.add(estados);
		estadosAlmacenados.add(estados);
		
		while(!listaEstados.isEmpty()) {
			Set<Integer> aux = listaEstados.get(0);
			
			TransicionMultiple tm = new TransicionMultiple();
			for (Integer a : aux) {
				for (Transicion t : this.AFNDe.getTransiciones()) {
					estados = new LinkedHashSet<>();
					tm = new TransicionMultiple();
					if(a == t.getEstado1() && t.getInput() != 'E') {
						
						tm.setEstado1(aux);
						tm.setInput(t.getInput());
						estados.add(t.getEstado2());
						estados.addAll(this.AFNDe.getClausuras().get(t.getEstado2()));
						tm.setEstado2(estados);
						
						if(!tieneEstadoEInput(tm)) {
							this.transicionesMultiples.add(tm);
						}
						else {
							agregarEstados(tm);
						}
						if(!this.estadosAlmacenados.contains(tm.getEstado2())) {
							this.estadosAlmacenados.add(tm.getEstado2());
							listaEstados.add(tm.getEstado2());
						}
					}
				}
			}
			listaEstados.remove(0);
		}
		System.out.println("****Paso intermedio, con multiples transiciones****");
		for (TransicionMultiple t : this.transicionesMultiples) {
			System.out.println(t);
		}
		System.out.println();
	}
	
	private void agregarEstados(TransicionMultiple tm) {
		for (TransicionMultiple transicionMultiple : this.transicionesMultiples) {
			if(tm.getEstado1().equals(transicionMultiple.getEstado1()) && tm.getInput() == transicionMultiple.getInput()) {
				transicionMultiple.setEstado2(tm.getEstado2());
				tm.setEstado2(transicionMultiple.getEstado2());
			}
		}
	}

	private boolean tieneEstadoEInput(TransicionMultiple tm) {
		for (TransicionMultiple transicionMultiple : this.transicionesMultiples) {
			if(tm.getEstado1().equals(transicionMultiple.getEstado1()) && tm.getInput() == transicionMultiple.getInput())
				return true;
		}
		return false;
	}
	
	private void agregarEstadosFinales() {
		if(!this.transicionesMultiples.isEmpty()) {
			for (TransicionMultiple tm : this.transicionesMultiples) {
				for (Integer e : tm.getEstado1()) {
					if(this.AFNDe.getEstadosFinales().contains(e)) {
						tm.setContieneEstadoFinal(true);
					}
				}
			}
		}
//		for (TransicionMultiple t : this.transicionesMultiples) {
//			System.out.println(t);
//		}
	}
	
	private Map<Set<Integer>,Integer> obtenerNuevosEstados() {
		int i = 1;
		Map<Set<Integer>,Integer> nuevos = new HashMap<>();
		for (Set<Integer> set : estadosAlmacenados) {
			nuevos.put(set,i);
			i++;
		}
		return nuevos;
	}
	
	private void obtenerNuevasTransiciones() {
		Transicion t ;
		int estado1 , estado2;
		char input;
		for (TransicionMultiple tm : transicionesMultiples) {
			estado1 = this.nuevosEstados.get(tm.getEstado1());
			input = tm.getInput();
			estado2 = this.nuevosEstados.get(tm.getEstado2());
			t = new Transicion(estado1, input, estado2);
			this.transiciones.add(t);
		}
		
//		for (Transicion tn : this.transiciones) {
//			System.out.println(tn);
//		}
	}
	
	private void obtenerNuevosEstadosFinales() {
		for (TransicionMultiple tm : transicionesMultiples) {
			if(tm.isContieneEstadoFinal()) {
				this.estadosFinales.add(this.nuevosEstados.get(tm.getEstado1()));
			}
		}
//		System.out.println(this.estadosFinales);
	}

	public Set<Character> getInputs() {
		return inputs;
	}

	public Integer getCantEstados() {
		return cantEstados;
	}

	public Set<Integer> getEstadosFinales() {
		return estadosFinales;
	}

	public Set<Transicion> getTransiciones() {
		return transiciones;
	}


	@Override
	public String toString() {
		String s = this.inputs + "\n";
		s += this.cantEstados + "\n";
		s += this.estadosFinales + "\n";
		for (Transicion t : this.transiciones) {
			s += t +"\n";
		}
		return s;
	}
	
	

}
