package tp.teoria.de.la.computacion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.swing.JOptionPane;

import org.apache.commons.math3.util.Pair;

import algoritmoCYK.CYK;
import interpreteDeProducciones.Interprete;
import lectorDeArchivos.LectorDeArchivos;
import normalizadorFNC.NormalizadorFNC;

public class Main {

	public static void main(String[] args) {
		
		//Producciones Ejemplo.txt
//		String ruta = "Producciones ejemplo.txt";
		
		//Producciones Ejemplo1.txt
//		String ruta = "Producciones ejemplo1.txt";
		
		//Producciones Ejemplo2.txt
//		String ruta = "Producciones ejemplo2.txt";
		
	
			
/*		
		ArrayList<String> resultado = LectorDeArchivos.leerArchivo(ruta);		
		Interprete interp = new Interprete();
		try {
			HashMap<String,ArrayList<String>> prod = interp.interpretarProducciones(resultado);		
			NormalizadorFNC n = new NormalizadorFNC(prod); 	
			
			
			System.out.println("-----------------------------------------------------------------");
			
			
			HashMap<String,ArrayList<String>> sinNulleables  = n.getResultadoSinNulleables();
			
			System.out.println("Producciones sin nulleables");
			
			for (Entry<String, ArrayList<String>> entry : sinNulleables.entrySet()) {
			    String key = entry.getKey();
			    ArrayList<String> value = entry.getValue();	    
			    for(String s : value) {
			    	System.out.println(key + " --> " + s );
			    }		    
			}
			
			
			
			
		System.out.println("-----------------------------------------------------------------");
			
			
			HashMap<String,ArrayList<String>> sinUnitarias  = n.getResultadoSinUnitarias();
			
			System.out.println("Producciones sin unitarias");
			
			for (Entry<String, ArrayList<String>> entry : sinUnitarias.entrySet()) {
			    String key = entry.getKey();
			    ArrayList<String> value = entry.getValue();	    
			    for(String s : value) {
			    	System.out.println(key + " --> " + s );
			    }		    
			}
	
			

			System.out.println("-----------------------------------------------------------------");
			
			
			HashMap<String,ArrayList<String>> res  = n.getResultadoSoloGeneradores();
			
			System.out.println("Producciones con solo generadores");
			
			for (Entry<String, ArrayList<String>> entry : res.entrySet()) {
			    String key = entry.getKey();
			    ArrayList<String> value = entry.getValue();	    
			    for(String s : value) {
			    	System.out.println(key + " --> " + s );
			    }		    
			}
		
			
			System.out.println("-----------------------------------------------------------------");
			
			
			HashMap<String,ArrayList<String>> resAlc  = n.getResultadoSoloAlcanzables();
			
			System.out.println("Producciones con solo alcanzables");
			
			for (Entry<String, ArrayList<String>> entry : resAlc.entrySet()) {
			    String key = entry.getKey();
			    ArrayList<String> value = entry.getValue();	    
			    for(String s : value) {
			    	System.out.println(key + " --> " + s );
			    } 
			}
	
			
			
			
			System.out.println("-----------------------------------------------------------------------------------------");
			
			
			ArrayList<Pair<String,String>> resultadoFinal = n.getTuplasFNC();
			
			System.out.println("Gramatica Final FNC");
			
			for(Pair<String,String> p : resultadoFinal) {
				System.out.println(p);
			}
	
			
			
			
			
			System.out.println("-----------------------------------------------------------------------------------------");
			System.out.println("-----------------------------------------------------------------------------------------");
			System.out.println("--------------------------------------Resultado parsing---------------------------------------------------");
			System.out.println("-----------------------------------------------------------------------------------------");
			System.out.println("-----------------------------------------------------------------------------------------");
					
			//Producciones Ejemplo.txt
	//		String input = "ababa";
			
			//Producciones Ejemplo1.txt
	//		String input = "aa";
			
			//Producciones Ejemplo2.txt
	//		String input = "ba@ab";
	
			
	
	//		String input = "bbbk";
			
			CYK algoritmo = new CYK(resultadoFinal, input);
	
	
			if(algoritmo.parserCyk()) {
				System.out.println("Lo reconoce");
			}
			else {
				System.out.println("No lo reconoce");
			}
			
			
			
			System.out.println("---------------------------------------------------------------------------------------------------");
			
			ArrayList<String> arrayArchivo = LectorDeArchivos.leerArchivo("ProduccionesVacioTest.txt");  
			
			
			for(String s : arrayArchivo) {
				System.out.println(s);
			}
			
		
		
		}
		catch(Exception e){
			System.out.println("Se produjo un error inesperado:");
			System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "Se produjo un error inesperado:", "ERROR_MESSAGE", 0);
			JOptionPane.showMessageDialog(null, e.getMessage());
		}	
*/		
			
	}

	
	
}


/*		NOTA: PARA EL NORMALIZADOR HABR�A QUE TOMAR EL ARRAY DE STRINGS Y POR CADA PRODUCCI�N QUE TENGA UN LARGO MAYOR A 2, 
  			  CORTAR LA PRIMERA POSICI�N, CREAR UNA VARIABLE NUEVA Y CONCATENAR ESOS 2 CHARS Y GUARDARLOS EN HASHMAP, 
  			  Y TODO LO QUE SIGUE AGREGARLO AL ARRAY DE LA NUEVA VARIABLE 		*/

