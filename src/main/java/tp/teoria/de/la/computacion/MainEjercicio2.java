package tp.teoria.de.la.computacion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import automatas.AFD;
import automatas.InterpreteAutomata;
import automatas.TransformarADF;
import lectorDeArchivos.LectorDeArchivos;

public class MainEjercicio2 {

	public static void main(String[] args) {		
		
		
		String ruta = "automatas/automata3.txt";
		InterpreteAutomata a = new InterpreteAutomata(ruta);
		
		System.out.println("*Resultado del archivo: AFND-e*");
		System.out.println(a.toString());
		
		System.out.println("**Clausuras**");
		System.out.println(a.getClausuras());
		System.out.println();
		
		TransformarADF tafd = new TransformarADF(a);
		System.out.println("*Resultado de la conversión: AFD");
		System.out.println(tafd.toString());
		
		
		AFD afd = new AFD(tafd);
//		String prueba = "aaab";
//		String prueba = "acc";
//		String prueba = "cb";
//		String prueba = "abfa";
		String prueba = "acabcbbb";
		System.out.println("Procesar el el String: " + prueba);
		if(afd.procesar(prueba)) {
			System.out.println("Acepta el String: " + prueba);
		}
		else {
			System.out.println("No acepta el String: " + prueba);
		}
//		Set<Character> inputs = tafd.getInputs();
//		Integer cantEstados = tafd.getCantEstados();
//		Set<Integer> estadosFinales = tafd.getEstadosFinales();
//		Set<Transicion> transiciones = tafd.getTransiciones();
//		
//		AFD afd = new AFD(inputs, cantEstados, estadosFinales, transiciones);
//		
//		System.out.println(afd.procesar("acgcbbab"));
		
//		System.out.println(inputs);
//		System.out.println(cantEstados);
//		System.out.println(estadosFinales);
//		for (Transicion transicion : transiciones) {
//			System.out.println(transicion);
//		}
		
//		Set<Set<Integer>> sets = new LinkedHashSet<>();
//		Set<Integer> set = new LinkedHashSet<>();
//		set.add(1);
//		set.add(2);
//		set.add(3);
//		sets.add(set);
//		set = new LinkedHashSet<>();
//		set.add(2);
//		set.add(3);
//		set.add(1);
//		sets.add(set);
//		System.out.println(sets);
		
//		for (Transicion t : a.getTransiciones()) {
//			System.out.println(t);
//		}
		
//		System.out.println("Clausuras");
//		Iterator it = a.getClausuras().keySet().iterator();
//		while (it.hasNext()) {
//			Integer key = (Integer) it.next();
//			System.out.print("Clave: " + key + " -> Valor: " );
//			for (Integer v : a.getClausuras().get(key)) {
//				System.out.print(" "+ v);
//			}
//			System.out.println();
//		}

	}

}
