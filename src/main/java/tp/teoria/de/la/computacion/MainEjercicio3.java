package tp.teoria.de.la.computacion;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import lectorDeArchivos.LectorDeArchivos;
import parserslr0.AFD;
import parserslr0.AnalizadorLR0;
import parserslr0.Estado;
import parserslr0.InterpreteGLC;
import parserslr0.Item;
import parserslr0.Transicion;
import parserslr0.utils.Util;
import parserslr0.utils.Validador;

public class MainEjercicio3
{

	public static void main(String[] args)
	{
		String ruta = "parsers/GLCaParserLR(0)1.txt";
		List<String> archivo = LectorDeArchivos.leerArchivo(ruta);
		
		InterpreteGLC glc = new InterpreteGLC(archivo);
	
		
		System.out.println("****Contenido del archivo: *****");
		for (String s : archivo) {
			System.out.println(s);
		}
		System.out.println();
		System.out.println("Gramatica obtenida: ");
		System.out.println(glc.getGLC());
//		System.out.println();
		AnalizadorLR0 c = new AnalizadorLR0(glc);
		System.out.println("----------------------------------------------------");
		System.out.println("Interprete: ");
		System.out.println("Simbolos: "+ c.getSimbolos());
		System.out.println();
		System.out.println("Items: ");
		for (Item item : c.getItems()) {
			System.out.println(item);
		}
		
		
		System.out.println("Clausura: " + c.calcularClausura(new Item("X_{0}",".X_{1}")));
		Set<Item> items = new LinkedHashSet<>();
		items.add(new Item("X_{1}","a.X_{1}b"));
		items.add(new Item("X_{1}",".aX_{1}b"));
		items.add(new Item("X_{1}",".c"));
		System.out.println("ir a: " + c.ir_a(items, "X_{1}"));
		System.out.println("----------------------------------------------------");
		AFD afd = new AFD(c);
		System.out.println("Estados");
		for (Estado e : afd.getEstados()) {
			System.out.println(e);
		}
		
		System.out.println("Transiciones: ");
		for (Transicion t : afd.getTransiciones()) {
			System.out.println(t);
		}
		
//		System.out.println(afd.construirAutomata());
//		afd.construirAutomata();
//		System.out.println(Util.desplazarPivot("aX_{1}b."));
//		System.out.println(Util.obtenerVariable(".b"));
//		for (Estado t : afd.agregarEstados()) {
//			System.out.println(t.getItems());
//		}
//		System.out.println(afd.agregarTransiciones());
		
		
//		Validador.validarProduccion("xX_{4} -> xX_{4}a |bX_{98}i | X_{98}xxx");
//		Validador.validarVariable("X_{10}");
//		Validador.validarBody("xX_{4}a | bX_{98}i |X_{98}xxx");
	}
	

}
