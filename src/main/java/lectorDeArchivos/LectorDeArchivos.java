package lectorDeArchivos;

import java.io.*;
import java.util.ArrayList;

public class LectorDeArchivos {
	
	public static ArrayList<String> leerArchivo(String ruta) {
		
		ArrayList<String> array = new ArrayList<String>();
		File archivo = null;
		FileReader fr = null;
		BufferedReader br = null;
		try {
		   archivo = new File (ruta);
		   fr = new FileReader (archivo);
		   br = new BufferedReader(fr);
		
		   String linea;
		   while((linea=br.readLine())!=null) {
			  array.add(linea);
		   }   
		}
		catch(Exception e){
//		   e.printStackTrace();
//			System.err.println("aca no hay una excepci�n");
		}
		finally{
		   try{                    
		      if( null != fr ){   
		         fr.close();     
		      }                  
		   }
		   catch (Exception e2){ 
		      e2.printStackTrace();
		   }
		}
		return array;
	}
}