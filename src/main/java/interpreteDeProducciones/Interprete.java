package interpreteDeProducciones;

import java.util.ArrayList;
import java.util.HashMap;

public class Interprete {

	public HashMap<String,ArrayList<String>> interpretarProducciones(ArrayList<String> array) {
		
		HashMap<String,ArrayList<String>> producciones = new HashMap<String,ArrayList<String>>();
		String clave = "";
		String pro;
		ArrayList<String> produc;
		
		for(String s : array) {	
			clave = "";
			pro = "";		
			produc = new ArrayList<String>();
			
			for(int i = 0; i < s.length(); i++) {
				if (clave == "" && s.charAt(i) != ' ') {
					clave += s.charAt(i);
				}
				else if ( (s.length() - 1) == i /* && s.charAt(i) != ' ' */ ) {
					if(s.charAt(i) != ' ') {
						pro += s.charAt(i);
						produc.add(pro);
					}
					if(s.charAt(i) == ' ' && pro != "") {
						produc.add(pro);
					}
				}
				else if(s.charAt(i) == ' ' || s.charAt(i) == '-' || s.charAt(i) == '>') {
					continue;
				}
				else if(s.charAt(i) == '|') {
					produc.add(pro);
					pro = "";
				}
				else {
					pro += s.charAt(i);
				}	
			}
			
			if(producciones.containsKey(clave)){
				producciones.get(clave).addAll(produc);
			}
			else {
				producciones.put(clave, produc);
			}	
		}
		
		boolean simboloInicial = false;
		for ( String key : producciones.keySet() ) {
		    if( key.equals("S") ) {
		    	simboloInicial = true;
		    }
		}
		if(!simboloInicial) {
			throw new RuntimeException("La gram�tica ingresada debe contener a 'S' como s�mbolo inicial");
		}
		
		return producciones;
	}
}