package parserslr0.utils;

import java.util.regex.Pattern;

public class Validador {

	public static void validarProduccion(String gramatica) {
		int j = 0;
		if(gramatica.contains(" -> ") && gramatica.length() != 4) {
			while(gramatica.charAt(j) != ' ') {
				j++;
			}
			validarVariable(gramatica.substring(0,j));
			validarBody(gramatica.substring(j+4,gramatica.length()));
		}
		else {
			throw new RuntimeException("Formato del archivo NO valido, Gramatica invalida");
		}
	}
	
	private static void validarVariable(String variable) {
		String transExp = "X_\\{[1-9][0-9]*\\}";
//		System.out.println(Pattern.matches(transExp, variable));
		
		
		if(!Pattern.matches(transExp, variable)){
			throw new RuntimeException("Formato del archivo NO valido, Gramatica invalida");
		}
	}
	
	private static void validarBody(String body){
		if(body.contains(" | ") && body.length() != 3) {
			
			int j = 0;
			while(body.charAt(j) != ' ') {
				j++;
			}
//			if(body.charAt(body.indexOf("|") +1) != ' ') {
//				throw new RuntimeException("Formato del archivo NO valido, Gramatica invalida | (barra sin espacio)");
//			}
			validarBody(body.substring(0,j));
			validarBody(body.substring(j+3,body.length()));
		}
		else {
			int j = 0;
			for (int i = 0; i < body.length(); i++) {
				if(body.charAt(i) == 'X') {
					j = i;
					while(body.charAt(j) != '}' && j < body.length()-1) {
						j++;
					}
					validarVariable(body.substring(i,j+1));
					i = j;
				}
				else {
					validarLetra(body.charAt(i)+"");
				}
				
			}
		}
		
	}
	
	private static void validarLetra(String letra) {
		String transExp = "[a-z�]";
//		System.out.println(letra);
		
		
		if(!Pattern.matches(transExp, letra)){
			throw new RuntimeException("Formato del archivo NO valido, Gramatica invalida");
		}
	}
	
	
}
