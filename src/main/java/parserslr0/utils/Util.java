package parserslr0.utils;

public class Util {
	
	public static String agregarPivot(int indice, String prod) {
		String body="";
		for (int i = 0; i < prod.length(); i++) {
			if(indice == i) {
				body+= "." + prod.charAt(i);
			}
			else {
				body+= "" + prod.charAt(i);
			}
		}
		return body;
	}

	public static String obtenerVariable(String body) {
		int j ;
		for (int i = 1; i < body.length(); i++) {
			if(body.charAt(i-1) == '.') {
				if(body.charAt(i) == 'X') {
					j = i;
					while(body.charAt(j) != '}') {
						j++;
					}
					return body.substring(i,j+1);
				}
				else 
					return body.charAt(i)+"";
			}
			
		}
		return null;
	}
	
	public static boolean tienePivot(String s) {
		return s.contains(".");
	}
	
	public static String desplazarPivot(String s) {
//		if(s.charAt(s.length()-1)== '.') {
//			return "mjs";
//		}
		if(s.length() <= 1) {
			return s;
		}
		else {
			if(s.charAt(0) == '.') {
				if(s.charAt(1) == 'X') {
					int j = 0;
					while(s.charAt(j) != '}') {
						j++;
					}
					return s.substring(1,j+1) + "." + desplazarPivot(s.substring(j+1, s.length()));
				}
				else return  s.charAt(1) +"."+desplazarPivot(s.substring(2));
			}
			return s.charAt(0) + desplazarPivot(s.substring(1));
		}
	}
	
}
