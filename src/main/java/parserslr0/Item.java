package parserslr0;

public class Item {

	private String variable;
	private String body;
	
	public Item(String variable, String body) {
		this.variable = variable;
		this.body = body;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((body == null) ? 0 : body.hashCode());
		result = prime * result + ((variable == null) ? 0 : variable.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item other = (Item) obj;
		if (body == null) {
			if (other.body != null)
				return false;
		} else if (!body.equals(other.body))
			return false;
		if (variable == null) {
			if (other.variable != null)
				return false;
		} else if (!variable.equals(other.variable))
			return false;
		return true;
	}

	public String getVariable() {
		return variable;
	}

	public String getBody() {
		return body;
	}

	@Override
	public String toString() {
		return "[" + variable + " -> " + body + "]";
	}
	
	
	
}
