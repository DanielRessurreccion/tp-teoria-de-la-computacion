package parserslr0;

import java.util.Set;

public class Estado {

	private Integer id;
	private Set<Item> items;
	private String asignacion;
	
	public Estado(Integer id, Set<Item> items) {
		this.id = id;
		this.items = items;
		this.asignacion = "?";
	}

	public Integer getId() {
		return id;
	}

	public Set<Item> getItems() {
		return items;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((asignacion == null) ? 0 : asignacion.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((items == null) ? 0 : items.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Estado other = (Estado) obj;
		if (asignacion == null) {
			if (other.asignacion != null)
				return false;
		} else if (!asignacion.equals(other.asignacion))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (items == null) {
			if (other.items != null)
				return false;
		} else if (!items.equals(other.items))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "[ I"+id+", " + items + "]";
	}
	
	
}
