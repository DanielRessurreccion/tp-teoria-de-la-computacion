package parserslr0;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import parserslr0.utils.Util;

public class AnalizadorLR0 {

	private Set<Item> items;
	private Map<String, Set<String>> producGLC;
	private Set<String> simbolos;
	
	
	public Set<Item> getItems() {
		return items;
	}

	public Map<String, Set<String>> getProducGLC() {
		return producGLC;
	}

	public Set<String> getSimbolos() {
		return simbolos;
	}

	public AnalizadorLR0(InterpreteGLC interprete) {
		this.producGLC = interprete.getGLC();
		items = new LinkedHashSet<>();
		this.agregarItems();
		this.simbolos = this.obtenerSimbolos();
//		for (Item item : items) {
//			System.out.println(item);
//		}
	}
	
	private Set<String> obtenerSimbolos() {
		Set<String> simbolos = new LinkedHashSet<>();
		int j;
		for (Entry<String, Set<String>> entry : this.producGLC.entrySet())
		{
			String clave = entry.getKey();
			simbolos.add(clave);
		    for(String prod : entry.getValue())
		    {
		    	for (int i = 0; i < prod.length(); i++) {
		    		j = i;
					if(prod.charAt(i) == 'X') {
						while(prod.charAt(j) != '}') {
							j++;
						}
						simbolos.add(prod.substring(i,j+1));
						i = j;
					}
					else
						simbolos.add(prod.charAt(i)+"");
				}
		    }
		}
		
		return simbolos;
	}
	
	private void agregarItems() {
		int j ;
		String body;
		for (Entry<String, Set<String>> entry : producGLC.entrySet())
		{
			j = 0;
			body = new String("");
			String clave = entry.getKey();
		    for(String prod : entry.getValue())
		    {
//		    	System.out.println(clave + " -> " + prod);
		    	if(prod.equals("�")) {
		    		this.items.add(new Item(clave,"."));
		    	}
		    	else {
		    		for (int i = 0; i < prod.length(); i++) {
						body = Util.agregarPivot(i,prod);
						j= i;
						if(prod.charAt(i) == 'X') {
							while(prod.charAt(j) != '}') {
								j++;
							}
							i = j;
						}
						this.items.add(new Item(clave,body));
					}
			    	this.items.add(new Item(clave,prod+"."));
		    	}
		    	
		    }
		    
		}
		System.out.println();
	}

	
	
	public Set<Item> calcularClausura(Item item) {
		
		if(this.items.contains(item)) {
			Set<Item> J = new LinkedHashSet<>();
			
			J.add(item);
			
			this.calcularClausura(J, item);
			return J;
		}
		
		return null;
	}
	
	private void calcularClausura(Set<Item> J, Item item) {
		String variable = Util.obtenerVariable(item.getBody());
		if(variable != null && variable.length() > 1) {
			for (Item item2 : this.items) {
				if(item2.getVariable().equals(variable) && item2.getBody().charAt(0) == '.') {
					J.add(item2);
					calcularClausura(J, item2);
				}
					
			}
		}
	}
	
	public Set<Item> ir_a(Set<Item> items, String simbolo) {
		Set<Item> J = new LinkedHashSet<>();
		for (Item item : items) {
			if(item.getBody().contains("."+simbolo)) {				
				J.addAll(this.calcularClausura(new Item(item.getVariable(), Util.desplazarPivot(item.getBody()))));
			}
		}
		return J;
	}

	
}
