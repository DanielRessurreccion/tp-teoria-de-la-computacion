package parserslr0;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import parserslr0.utils.Validador;

public class InterpreteGLC
{
	
	private Map<String, Set<String>> producGLC;
	
	private List<String> producciones;
	
	public InterpreteGLC(List<String> archivo)
	{
		this.producciones = archivo;
		producGLC = new HashMap<String, Set<String>>();
		for (String produccion: archivo)
		{
			Validador.validarProduccion(produccion);
		}
		
		this.agreggarProducciones();
	}
	
	private void agreggarProducciones() {
		
		this.agregarSimboDistinguido();
		
		String clave = "";
		Set<String> valores;
		for (String p : this.producciones) {
			clave = p.substring(0,p.indexOf('}')+1);
			valores = this.agregarValores(p.substring(p.indexOf('}')+5,p.length()));
			if(this.producGLC.containsKey(clave)) {
				this.producGLC.get(clave).addAll(valores);
			}
			else
				this.producGLC.put(clave, valores);
		}
		
	}
	
	private void agregarSimboDistinguido() {
		String clave = "X_{0}";  // Se asume como simbolo distinguido.
		String produc = "X_{1}"; // por definicion de enunciado se toma como simbolo inicial.
		Set<String> listaProduc = new LinkedHashSet<String>();
		listaProduc.add(produc);
		this.producGLC.put(clave, listaProduc);
	}

	private Set<String> agregarValores(String s) {
		Set<String> valores = new LinkedHashSet<>();
		String valor = "";
		for (int i = 0; i < s.length(); i++) {
			if(s.charAt(i) == ' ' || s.charAt(i) == '|') {
				if(!valor.isEmpty()) {
					valores.add(valor);
					valor = "";
				}
			}
			else {
				valor = valor + s.charAt(i);
			}
		}
//		if(!valor.isEmpty())
			valores.add(valor);
		return valores;
		
	}
	
	public Map<String, Set<String>> getGLC()
	{
		return producGLC;
	}
	
}
