package parserslr0;

import java.util.*;

import parserslr0.utils.Util;

public class AFD {
	
	private AnalizadorLR0 lr ;
	private Set<Transicion> transiciones;
	private Set<Estado> estados;

	public AFD(AnalizadorLR0 lr) {
		this.lr = lr;
		this.estados = new LinkedHashSet<>();
		this.transiciones = new LinkedHashSet<>();
		this.agregarEstados();
		this.agregarTransiciones();
	}
	
	private void agregarEstados() {
		Set<Estado> C = new LinkedHashSet<>();
		List<Estado> aux = new LinkedList<>();
		int id = 0;
		C.add(new Estado(1,lr.calcularClausura(new Item("X_{0}",".X_{1}"))));
		aux.add(new Estado(1,lr.calcularClausura(new Item("X_{0}",".X_{1}"))));
		this.estados.add(new Estado(id,lr.calcularClausura(new Item("X_{0}",".X_{1}"))));
		id++;
		
		while(!aux.isEmpty()) {
			Estado c = aux.get(0);
			for (String s : lr.getSimbolos()) {
				if(!lr.ir_a(c.getItems(),s).isEmpty()) {
					if(!C.contains(new Estado(1,lr.ir_a(c.getItems(),s)))) {
						C.add(new Estado(1,lr.ir_a(c.getItems(),s)));
						aux.add(new Estado(1,lr.ir_a(c.getItems(),s)));
						this.estados.add(new Estado(id,lr.ir_a(c.getItems(),s)));
						id++;
					}
				}
			}
			aux.remove(0);
		}
		
//		return this.estados;
	}
	
	private void agregarTransiciones(){
		Transicion t;
		for (Estado estado : estados) {
			for (Item item : estado.getItems()) {
				for (Estado estado2 : estados) {
					for (Item item2 : estado2.getItems()) {
						if(item.getBody().charAt(item.getBody().length()-1) != '.') {
							if(Util.desplazarPivot(item.getBody()).equals(item2.getBody())) {
								t = new Transicion(estado, Util.obtenerVariable(item.getBody()), estado2);
								this.transiciones.add(t);
							}
						}
						break;
					}
				}
			}
		}
//		return transiciones;
		
	}
	
	public Set<Estado> getEstados() {
		return estados;
	}

	public Set<Transicion> getTransiciones() {
		return transiciones;
	}

}
