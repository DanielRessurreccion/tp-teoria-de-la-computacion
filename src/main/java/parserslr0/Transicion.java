package parserslr0;

public class Transicion {
	
	private Estado estado1;
	private String simbolo;
	private Estado estado2;
	
	public Transicion(Estado estado1, String simbolo, Estado estado2) {
		this.estado1 = estado1;
		this.simbolo = simbolo;
		this.estado2 = estado2;
	}

//	public Estado getEstado1() {
//		return estado1;
//	}
//
//	public String getSimbolo() {
//		return simbolo;
//	}
//
//	public Estado getEstado2() {
//		return estado2;
//	}
//
	@Override
	public String toString() {
		return "[" + estado1 + ", " + simbolo + " -> " + estado2 + "]\n";
	}
	
	

}
