package automatas;

import static org.junit.Assert.*;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

public class InterpreteAutomataTest {

	String ruta1 = "automatas/automata1.txt";
	String ruta3 = "automatas/automata3.txt";
	InterpreteAutomata interprete1 = new InterpreteAutomata(ruta1);
	InterpreteAutomata interprete3 = new InterpreteAutomata(ruta3);
	InterpreteAutomata interpreteNovalido ;
//	InterpreteAutomata interpreteNovalido1 = new InterpreteAutomata("automatas/noValido1.txt");
	
	@Test(expected=RuntimeException.class)
	public void inputInvalidoTest() {
		interpreteNovalido = new InterpreteAutomata("automatas/inputNoValido.txt");
	}
	
	@Test(expected=RuntimeException.class)
	public void estadosFinalesNoValidosTest() {
		interpreteNovalido = new InterpreteAutomata("automatas/EstadosFinalesNoValido.txt");
	}
	
	@Test(expected=NumberFormatException.class)
	public void transicionNoValidoTest() {
		interpreteNovalido = new InterpreteAutomata("automatas/cantEstadosNoValidos.txt");
	}
	
	@Test
	public void getInputsTest() {
		Set<Character> getInputs = new LinkedHashSet<>();
		getInputs.add('a');
		getInputs.add('b');
		getInputs.add('c');
		assertEquals(getInputs,interprete1.getInputs());
		assertEquals(3,interprete3.getInputs().size());
		assertEquals(getInputs,interprete1.getInputs());
		assertEquals(3,interprete3.getInputs().size());
	}
	
	@Test
	public void getCantEstadosTest() {
		Integer n = 3;
		Integer m = 6;
		assertEquals(n,interprete1.getCantEstados());
		assertEquals(m,interprete3.getCantEstados());
	}
	
	@Test
	public void getEstadosFinalesTest() {
		Set<Integer> getEstadosFinales = new LinkedHashSet<>();
		getEstadosFinales.add(3);
		assertEquals(getEstadosFinales,interprete1.getEstadosFinales());
		assertEquals(1,interprete1.getEstadosFinales().size());
		getEstadosFinales.add(5);
		assertEquals(getEstadosFinales,interprete3.getEstadosFinales());
		assertEquals(2,interprete3.getEstadosFinales().size());
	}
	
	@Test
	public void getTransicionesTest() {
		Set<Transicion> getTransiciones = new LinkedHashSet<>();
		getTransiciones.add(new Transicion(1,'a',1));
		getTransiciones.add(new Transicion(1,'b',2));
		getTransiciones.add(new Transicion(1,'c',3));
		getTransiciones.add(new Transicion(2,'E',1));
		getTransiciones.add(new Transicion(2,'a',2));
		getTransiciones.add(new Transicion(2,'b',3));
		getTransiciones.add(new Transicion(3,'E',2));
		getTransiciones.add(new Transicion(3,'a',3));
		getTransiciones.add(new Transicion(3,'c',1));
		
		assertEquals(getTransiciones,interprete1.getTransiciones());
		assertEquals(9,interprete1.getTransiciones().size());
		assertEquals(12,interprete3.getTransiciones().size());
	}
	
	@Test
	public void getClausurasTest() {
		Map<Integer, Set<Integer>> getClausuras = new LinkedHashMap<>();
		Set<Integer> nums = new LinkedHashSet<>();
		nums.add(1);
		getClausuras.put(1, nums);
		nums = new LinkedHashSet<>();
		nums.add(2);
		nums.add(1);
		getClausuras.put(2, nums);
		nums = new LinkedHashSet<>();
		nums.add(3);
		nums.add(2);
		nums.add(1);
		getClausuras.put(3, nums);
		assertEquals(getClausuras,interprete1.getClausuras());
		
		assertEquals(1,interprete1.getClausuras().get(1).size());
		assertEquals(2,interprete1.getClausuras().get(2).size());
		assertEquals(3,interprete1.getClausuras().get(3).size());
		
		assertEquals(1,interprete3.getClausuras().get(1).size());
		assertEquals(1,interprete3.getClausuras().get(2).size());
		assertEquals(2,interprete3.getClausuras().get(3).size());
		assertEquals(1,interprete3.getClausuras().get(4).size());
		assertEquals(1,interprete3.getClausuras().get(5).size());
		assertEquals(1,interprete3.getClausuras().get(6).size());
	}
	
	@Test
	public void toStringTest() {
		assertEquals(this.esperado1(),interprete1.toString());
		assertEquals(this.esperado3(),interprete3.toString());
		
	}
	
	String esperado1() {
		return 	"[a, b, c]\n"+
				"3\n"+
				"[3]\n"+
				"1, a -> 1\n"+
				"1, b -> 2\n"+
				"1, c -> 3\n"+
				"2, E -> 1\n"+
				"2, a -> 2\n"+
				"2, b -> 3\n"+
				"3, E -> 2\n"+
				"3, a -> 3\n"+
				"3, c -> 1\n";
	}
	
	String esperado3() {
		return "[a, b, c]\n"+
				"6\n"+
				"[3, 5]\n"+
				"1, a -> 2\n"+
				"2, c -> 3\n"+
				"3, b -> 3\n"+
				"3, c -> 3\n"+
				"3, E -> 4\n"+
				"4, a -> 5\n"+
				"5, a -> 5\n"+
				"5, c -> 3\n"+
				"5, b -> 6\n"+
				"6, a -> 5\n"+
				"6, b -> 3\n"+
				"6, c -> 3\n";
	}
	

}
