package automatas;

import static org.junit.Assert.*;

import java.util.LinkedHashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class AFDTest {
	
	String ruta1 = "automatas/automata1.txt";
	String ruta3 = "automatas/automata3.txt";
	InterpreteAutomata interprete1;
	InterpreteAutomata interprete3;
	TransformarADF tafd1;
	TransformarADF tafd3;
	AFD afd1 ;
	
	@Before
	public void init() {
		interprete1 = new InterpreteAutomata(ruta1);
		interprete3 = new InterpreteAutomata(ruta3);
		tafd1 = new TransformarADF(interprete1);
		tafd3 = new TransformarADF(interprete3);
		afd1 = new AFD(tafd1);
	}

	@Test
	public void procesarStringValidoTest() {
		assertTrue(afd1.procesar("aaaabbbbcccc"));
	}
	
	@Test
	public void procesarStringNoAceptadoTest() {
		assertFalse(afd1.procesar("aaaab"));
	}
	
	@Test
	public void procesarStringInputNoValidoTest() {
		assertFalse(afd1.procesar("abcFabc"));
	}
	
	@Test
	public void procesarStringProximoEstadoNullTest() {
		afd1.getTransiciones().clear();
		assertFalse(afd1.procesar("abcFabc"));
	}
	
	@Test
	public void getCantEstadosTest() {
		assertEquals((Integer)3,afd1.getCantEstados());
	}
	
	@Test
	public void getEstadosFinalesTest() {
		Set<Integer> getEstadosFinales = new LinkedHashSet<>();
		getEstadosFinales.add(3);
		assertEquals(getEstadosFinales,afd1.getEstadosFinales());
		assertEquals(1,afd1.getEstadosFinales().size());
	}
	
	@Test
	public void getInputsTest() {
		Set<Character> getInputs = new LinkedHashSet<>();
		getInputs.add('a');
		getInputs.add('b');
		getInputs.add('c');
		assertEquals(getInputs,afd1.getInputs());
		assertEquals(3,afd1.getInputs().size());
	}

}
