package automatas;

import static org.junit.Assert.*;

import java.util.LinkedHashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class TransformarAFDTest {
	
	String ruta1 = "automatas/automata1.txt";
	String ruta3 = "automatas/automata3.txt";
	InterpreteAutomata interprete1;
	InterpreteAutomata interprete3;
	TransformarADF tafd1;
	TransformarADF tafd3;
	
	@Before
	public void init() {
		interprete1 = new InterpreteAutomata(ruta1);
		interprete3 = new InterpreteAutomata(ruta3);
		tafd1 = new TransformarADF(interprete1);
		tafd3 = new TransformarADF(interprete3);
	}

	@Test
	public void getInputsTest() {
		Set<Character> getInputs = new LinkedHashSet<>();
		getInputs.add('a');
		getInputs.add('b');
		getInputs.add('c');
		assertEquals(getInputs,tafd1.getInputs());
		assertEquals(3,tafd1.getInputs().size());
		assertEquals(getInputs,tafd3.getInputs());
		assertEquals(3,tafd3.getInputs().size());
	}
	
	@Test
	public void getCantEstadosTest() {
		Integer n = 3;
		Integer m = 5;
		assertEquals(n,tafd1.getCantEstados());
		assertEquals(m,tafd3.getCantEstados());
	}
	
	@Test
	public void getEstadosFinalesTest() {
		Set<Integer> getEstadosFinales = new LinkedHashSet<>();
		getEstadosFinales.add(3);
		assertEquals(getEstadosFinales,tafd1.getEstadosFinales());
		assertEquals(1,tafd1.getEstadosFinales().size());
		getEstadosFinales.add(4);
		assertEquals(getEstadosFinales,tafd3.getEstadosFinales());
		assertEquals(2,tafd3.getEstadosFinales().size());
	}
	
	@Test
	public void getTransicionesTest() {
		
		Set<Transicion> getTransiciones = new LinkedHashSet<>();
		getTransiciones.add(new Transicion(1,'a',1));
		getTransiciones.add(new Transicion(1,'b',2));
		getTransiciones.add(new Transicion(1,'c',3));
		getTransiciones.add(new Transicion(2,'a',2));
		getTransiciones.add(new Transicion(2,'b',3));
		getTransiciones.add(new Transicion(2,'c',3));
		getTransiciones.add(new Transicion(3,'a',3));
		getTransiciones.add(new Transicion(3,'b',3));
		getTransiciones.add(new Transicion(3,'c',3));
		
		assertEquals(getTransiciones,tafd1.getTransiciones());
		assertEquals(9,tafd1.getTransiciones().size());
		assertEquals(11,tafd3.getTransiciones().size());
	}
	
	@Test
	public void toStringTest() {
		assertEquals(this.esperado1(),tafd1.toString());
		assertEquals(this.esperado3(),tafd3.toString());
		
	}
	
	String esperado1() {
		return 	"[a, b, c]\n"+
				"3\n"+
				"[3]\n"+
				"1, a -> 1\n"+
				"1, b -> 2\n"+
				"1, c -> 3\n"+
				"2, a -> 2\n"+
				"2, b -> 3\n"+
				"2, c -> 3\n"+
				"3, a -> 3\n"+
				"3, c -> 3\n"+
				"3, b -> 3\n";
	}
	
	String esperado3() {
		return "[a, b, c]\n"+
				"5\n"+
				"[3, 4]\n"+
				"1, a -> 2\n"+
				"2, c -> 3\n"+
				"3, b -> 3\n"+
				"3, c -> 3\n"+
				"3, a -> 4\n"+
				"4, a -> 4\n"+
				"4, c -> 3\n"+
				"4, b -> 5\n"+
				"5, a -> 4\n"+
				"5, b -> 3\n"+
				"5, c -> 3\n";
	}

}
