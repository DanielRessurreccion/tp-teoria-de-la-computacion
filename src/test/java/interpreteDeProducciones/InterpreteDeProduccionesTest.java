package interpreteDeProducciones;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import lectorDeArchivos.LectorDeArchivos;

public class InterpreteDeProduccionesTest {

	Interprete interprete;
	HashMap<String,ArrayList<String>> resExitoso;
	HashMap<String,ArrayList<String>> comparativa;
	ArrayList<String> arr1;
	ArrayList<String> arr2;
	ArrayList<String> arr3;
	
	@Before
	public void iniciar(){
		interprete = new Interprete();
		comparativa = new HashMap<String,ArrayList<String>>();
		arr1 = new ArrayList<String>();
		arr2 = new ArrayList<String>();
		arr3 = new ArrayList<String>();
		arr1.add("bAb");
		arr1.add("aBa");
		arr2.add("a");
		arr3.add("b");
		comparativa.put("S", arr1);
		comparativa.put("A", arr2);		
		comparativa.put("B", arr3);		
	}
	
	@Test
	public void interpretacionExitosa() {
 		resExitoso = interprete.interpretarProducciones(LectorDeArchivos.leerArchivo("ProduccionesTestExitoso1.txt"));
		assertEquals(comparativa,resExitoso);
	}

	@Test
	public void produccionesClaveRepetidaTest() {
		resExitoso = interprete.interpretarProducciones(LectorDeArchivos.leerArchivo("ProduccionesVariableRepetidaTest.txt"));
		assertEquals(comparativa, resExitoso);
	}
	
	@Test(expected=RuntimeException.class)
	public void interpretacionFallida(){
		HashMap<String,ArrayList<String>> resFallido = interprete.interpretarProducciones(LectorDeArchivos.leerArchivo("ProduccionesTestFallido1.txt"));
	}

}
