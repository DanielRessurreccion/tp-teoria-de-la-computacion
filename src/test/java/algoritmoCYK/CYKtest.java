package algoritmoCYK;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.math3.util.Pair;
import org.junit.Before;
import org.junit.Test;

import interpreteDeProducciones.Interprete;
import lectorDeArchivos.LectorDeArchivos;
import normalizadorFNC.NormalizadorFNC;

public class CYKtest {
	
	NormalizadorFNC norm;
	NormalizadorFNC norm1;
	NormalizadorFNC norm2;
	NormalizadorFNC norm3;
	
	@Before
	public void iniciar() {
		Interprete interp = new Interprete();
		
		String ruta = "Producciones ejemplo.txt";
		ArrayList<String> resultado = LectorDeArchivos.leerArchivo(ruta);		
		HashMap<String,ArrayList<String>> prod = interp.interpretarProducciones(resultado);		
		norm = new NormalizadorFNC(prod); 
		
		String ruta1 = "Producciones ejemplo1.txt";
		ArrayList<String> resultado1 = LectorDeArchivos.leerArchivo(ruta1);		
		HashMap<String,ArrayList<String>> prod1 = interp.interpretarProducciones(resultado1);		
		norm1 = new NormalizadorFNC(prod1); 
		
		String ruta2 = "Producciones ejemplo2.txt";
		ArrayList<String> resultado2 = LectorDeArchivos.leerArchivo(ruta2);		
		HashMap<String,ArrayList<String>> prod2 = interp.interpretarProducciones(resultado2);		
		norm2 = new NormalizadorFNC(prod2); 
		
		String ruta3 = "Producciones ejemplo3.txt";
		ArrayList<String> resultado3 = LectorDeArchivos.leerArchivo(ruta3);		
		HashMap<String,ArrayList<String>> prod3 = interp.interpretarProducciones(resultado3);		
		norm3 = new NormalizadorFNC(prod3); 
	}	
	
	@Test
	public void parseoCYKcasoFeliz() {
		String input = "abab";
		ArrayList<Pair<String,String>> resultadoFinal = norm.getTuplasFNC();
		CYK algoritmo = new CYK(resultadoFinal, input);
		assertTrue(algoritmo.parserCyk());
	}
	
	@Test
	public void parseoCYKcasoTriste() {
		String input = "aaaa";
		ArrayList<Pair<String,String>> resultadoFinal = norm.getTuplasFNC();
		CYK algoritmo = new CYK(resultadoFinal, input);
		assertFalse(algoritmo.parserCyk());
	}

	@Test
	public void parseoCYKcasoFeliz1() {
		String input = "(a)";
		ArrayList<Pair<String,String>> resultadoFinal = norm1.getTuplasFNC();
		CYK algoritmo = new CYK(resultadoFinal, input);
		assertTrue(algoritmo.parserCyk());
	}
	
	@Test
	public void parseoCYKcasoTriste1() {
		String input = "ab01k";
		ArrayList<Pair<String,String>> resultadoFinal = norm1.getTuplasFNC();
		CYK algoritmo = new CYK(resultadoFinal, input);
		assertFalse(algoritmo.parserCyk());
	}

	@Test
	public void parseoCYKcasoFeliz2() {
		String input = "a@a";
		ArrayList<Pair<String,String>> resultadoFinal = norm2.getTuplasFNC();
		CYK algoritmo = new CYK(resultadoFinal, input);
		assertTrue(algoritmo.parserCyk());
	}
	
	@Test
	public void parseoCYKcasoTriste2() {
		String input = "a@b";
		ArrayList<Pair<String,String>> resultadoFinal = norm2.getTuplasFNC();
		CYK algoritmo = new CYK(resultadoFinal, input);
		assertFalse(algoritmo.parserCyk());
	}
	
	@Test
	public void parseoCYKcasoFeliz3() {
		String input = "bbbbk";
		ArrayList<Pair<String,String>> resultadoFinal = norm3.getTuplasFNC();
		CYK algoritmo = new CYK(resultadoFinal, input);
		assertTrue(algoritmo.parserCyk());
	}
	
	@Test
	public void parseoCYKcasoTriste3() {
		String input = "ak";
		ArrayList<Pair<String,String>> resultadoFinal = norm2.getTuplasFNC();
		CYK algoritmo = new CYK(resultadoFinal, input);
		assertFalse(algoritmo.parserCyk());
	}
	
}