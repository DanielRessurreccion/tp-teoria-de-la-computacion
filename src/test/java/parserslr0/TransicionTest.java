package parserslr0;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class TransicionTest {

	Set<Item> items; 
	Estado estado; 
	Transicion transicion;
	Transicion transicion2;
	
	@Before
	public void iniciar() {
		Item item1 = new Item("S","aA");
		Item item2 = new Item("A","a");
		items = new HashSet<Item>();
		items.add(item1);
		items.add(item2);
		estado = new Estado(1, items);
		transicion = new Transicion(estado,"a",estado);
		transicion2 = new Transicion(estado,"a",estado);
	}
	
	@Test
	public void toStringTest() {
//		String t = "[[ I1, [[A -> a], [S -> aA]]], a -> [ I1, [[A -> a], [S -> aA]]]]";
		
		assertEquals(transicion2.toString(),transicion.toString());
	}

}
