package parserslr0;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class EstadoTest {
	Set<Item> items; 
	Estado estado; 
	
	@Before
	public void iniciar() {
		Item item1 = new Item("S","aA");
		Item item2 = new Item("A","a");
		items = new HashSet<Item>();
		items.add(item1);
		items.add(item2);
		estado = new Estado(1, items);
	}
	
	@Test
	public void getIdTest() {
		int est = estado.getId();
		assertEquals(1, est);
	}
	
	@Test
	public void toStringTest() {
		String str = estado.toString();
		System.out.println(str);
		String comparar = "[ I1, [[A -> a], [S -> aA]]]";
		assertEquals(comparar, str);
	}
	
	@Test 
	public void setItemsTest() {
		Set<Item> itemsEst = estado.getItems();;
		assertEquals(items, itemsEst);
	}
	
	
}
