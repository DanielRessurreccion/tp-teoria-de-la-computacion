package parserslr0.utils;

import static org.junit.Assert.*;

import org.junit.Test;

public class UtilTest {
	
	@Test
	public void agregarPivotTest() {
		String s = Util.agregarPivot(0, "X_{1}");
		assertEquals(s,".X_{1}");
	}
	
	@Test
	public void obtenerVariableTest() {
		String s = Util.obtenerVariable("X_{1} -> a.X_{2}aa");
		assertEquals(s,"X_{2}");
	}
	
	@Test
	public void obtenerVariableNullTest() {
		String s = Util.obtenerVariable("X_{1} -> aaa");
		assertNull(s);
	}
	
	@Test
	public void obtenerVariable2Test() {
		String s = Util.obtenerVariable("X_{1} -> aX_{2}.aa");
		assertEquals(s,"a");
	}
	
	@Test
	public void tienePivotTrueTest() {
		assertTrue(Util.tienePivot("aX_{2}.aa"));
	}
	
	@Test
	public void tienePivotFalseTest() {
		assertFalse(Util.tienePivot("aX_{2}aa"));
	}
	
	@Test
	public void DesplazarPivotUnStringTest() {
		String s = Util.desplazarPivot("a");
		assertEquals(s,"a");
	}
	
	@Test
	public void DesplazarPivotVariableTest() {
		String s = Util.desplazarPivot("a.X_{2}b");
		assertEquals(s,"aX_{2}.b");
	}
	
	@Test
	public void DesplazarPivotLetraTest() {
		String s = Util.desplazarPivot(".aX_{2}b");
		assertEquals(s,"a.X_{2}b");
	}

}
