package parserslr0.utils;

import org.junit.Test;

public class ValidadorTest {

	@Test
	public void validarProduccion3Test() {
		Validador.validarProduccion("X_{1} -> aX_{2}b | bX_{3}");
	}
	
	@Test(expected=RuntimeException.class)
	public void validarProduccion1Test() {
		Validador.validarProduccion("sdds");
	}
	
	@Test(expected=RuntimeException.class)
	public void validarProduccion2Test() {
		Validador.validarProduccion("x -> x");
	}
	
	@Test(expected=RuntimeException.class)
	public void validarProduccion4Test() {
		Validador.validarProduccion(" -> ");
	}
	
	@Test(expected=RuntimeException.class)
	public void validarProduccion5Test() {
		Validador.validarProduccion("X_{1} -> aX_{2}B");
	}
	
	@Test(expected=RuntimeException.class)
	public void validarProduccion6Test() {
		Validador.validarProduccion("X_{1} -> aX_{2");
	}
	
	@Test(expected=RuntimeException.class)
	public void validarProduccion7Test() {
		Validador.validarProduccion("X_{1} ->  | ");
	}
	
//	@Test(expected=RuntimeException.class)
//	public void validarProduccion8Test() {
//		Validador.validarProduccion("xX_{4}a | bX_{98}i |X_{98}xxx}");
//	}
}
