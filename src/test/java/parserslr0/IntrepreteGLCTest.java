package parserslr0;

import static org.junit.Assert.*;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import lectorDeArchivos.LectorDeArchivos;

public class IntrepreteGLCTest {

	String ruta1 = "parsers/invalido.txt";
	List<String> archivo1 = LectorDeArchivos.leerArchivo(ruta1);
	InterpreteGLC glc1 ;
	
	String ruta2 = "parsers/GLCaParserLR(0)2.txt";
	List<String> archivo2 = LectorDeArchivos.leerArchivo(ruta2);
	InterpreteGLC glc2 ;
	
	String ruta3 = "parsers/GLCaParserLR(0)1.txt";
	List<String> archivo3 = LectorDeArchivos.leerArchivo(ruta3);
	InterpreteGLC glc3 ;
	
	@Before
	public void init() {
		glc2 = new InterpreteGLC(archivo2);
		glc3 = new InterpreteGLC(archivo3);
	}
	
	@Test(expected=RuntimeException.class)
	public void contenidoInvalidoTest() {
		glc1 = new InterpreteGLC(archivo1);
	}
	
	@Test
	public void creacionExitosaTest() {
		glc1 = new InterpreteGLC(archivo2);
	}
	
	@Test
	public void creacionExitosa2Test() {
		glc1 = new InterpreteGLC(archivo3);
	}
	
	@Test
	public void glcSizeTest() {
		Map<String, Set<String>> getGLC = new LinkedHashMap<String, Set<String>>();
		Set<String> bodys = new LinkedHashSet<>();
		bodys.add("X_{1}");
		getGLC.put("X_{0}", bodys);
		bodys = new LinkedHashSet<>();
		bodys.add("aX_{1}b");
		bodys.add("c");
		getGLC.put("X_{1}", bodys);
		assertEquals(getGLC,this.glc3.getGLC());
		
		assertEquals(2,this.glc3.getGLC().size());
		assertEquals(3,this.glc2.getGLC().size());
	}

}
