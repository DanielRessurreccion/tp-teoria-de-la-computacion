package parserslr0;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import lectorDeArchivos.LectorDeArchivos;

public class AnalizadorLR0Test {
	
	String ruta2 = "parsers/GLCaParserLR(0)2.txt";
	List<String> archivo2 = LectorDeArchivos.leerArchivo(ruta2);
	InterpreteGLC glc2 ;
	
	String ruta3 = "parsers/GLCaParserLR(0)1.txt";
	List<String> archivo3 = LectorDeArchivos.leerArchivo(ruta3);
	InterpreteGLC glc3 ;
	String ruta4 = "parsers/transicionEpsilon.txt";
	List<String> archivo4 = LectorDeArchivos.leerArchivo(ruta4);
	InterpreteGLC glc4 ;
	AnalizadorLR0 analizador2;
	AnalizadorLR0 analizador3;
	AnalizadorLR0 analizador4;
	
	@Before
	public void init() {
		glc2 = new InterpreteGLC(archivo2);
		glc3 = new InterpreteGLC(archivo3);
		glc4 = new InterpreteGLC(archivo4);
		analizador2 = new AnalizadorLR0(glc2);
		analizador3 = new AnalizadorLR0(glc3);
		analizador4 = new AnalizadorLR0(glc4);
	}

	@Test
	public void getItemTest() {
		Set<Item> getItems = new LinkedHashSet<>();
		getItems.add(new Item("X_{0}",".X_{1}"));
		getItems.add(new Item("X_{0}","X_{1}."));
		getItems.add(new Item("X_{1}",".aX_{1}b"));
		getItems.add(new Item("X_{1}","a.X_{1}b"));
		getItems.add(new Item("X_{1}","aX_{1}.b"));
		getItems.add(new Item("X_{1}","aX_{1}b."));
		getItems.add(new Item("X_{1}",".c"));
		getItems.add(new Item("X_{1}","c."));
		assertEquals(getItems,this.analizador3.getItems());
		
		assertEquals(9,this.analizador2.getItems().size());
		assertEquals(8,this.analizador3.getItems().size());
		assertEquals(6,this.analizador4.getItems().size());
	}
	
	@Test
	public void getProducGLCTest() {
		Map<String, Set<String>> getGLC = new LinkedHashMap<String, Set<String>>();
		Set<String> bodys = new LinkedHashSet<>();
		bodys.add("X_{1}");
		getGLC.put("X_{0}", bodys);
		bodys = new LinkedHashSet<>();
		bodys.add("aX_{1}b");
		bodys.add("c");
		getGLC.put("X_{1}", bodys);
		assertEquals(getGLC,this.analizador3.getProducGLC());
		
		assertEquals(3,this.analizador2.getProducGLC().size());// ESTOS SON DICCIONARIOS
		assertEquals(2,this.analizador3.getProducGLC().size());
		assertEquals(3,this.analizador4.getProducGLC().size());
	}
	
	@Test
	public void getSimbolosTest() {
		Set<String> getSimbolos = new LinkedHashSet<>();
		getSimbolos.add("X_{0}");
		getSimbolos.add("X_{1}");
		getSimbolos.add("a");
		getSimbolos.add("b");
		getSimbolos.add("c");
		assertEquals(getSimbolos,this.analizador3.getSimbolos());
		
		assertEquals(4,this.analizador2.getSimbolos().size());
		assertEquals(5,this.analizador3.getSimbolos().size());
		assertEquals(5,this.analizador4.getSimbolos().size());
	}
	
	@Test
	public void calcularClausuraTest() {
		Set<Item> calcularClausura = new LinkedHashSet<>();
		calcularClausura.add(new Item("X_{0}",".X_{1}"));
		calcularClausura.add(new Item("X_{1}",".aX_{1}b"));
		calcularClausura.add(new Item("X_{1}",".c"));
		assertEquals(calcularClausura,this.analizador3.calcularClausura(new Item("X_{0}",".X_{1}")));
		
		assertEquals(1,this.analizador4.calcularClausura(new Item("X_{2}",".")).size());
		assertNull(this.analizador4.calcularClausura(new Item("X_{2}","saraza")));
		assertEquals(3,this.analizador3.calcularClausura(new Item("X_{0}",".X_{1}")).size());
		assertEquals(4,this.analizador2.calcularClausura(new Item("X_{0}",".X_{1}")).size());
	}
	
	@Test
	public void ir_aTest() {
		Set<Item> items = new LinkedHashSet<>();
		items.add(new Item("X_{1}","a.X_{1}b"));
		items.add(new Item("X_{1}",".aX_{1}b"));
		items.add(new Item("X_{1}",".c"));
		
		Set<Item> ir_a = new LinkedHashSet<>();
		ir_a.add(new Item("X_{1}","aX_{1}.b"));
		assertEquals(ir_a,this.analizador3.ir_a(items, "X_{1}"));
		assertEquals(1,this.analizador3.ir_a(items, "X_{1}").size());

	}
}
