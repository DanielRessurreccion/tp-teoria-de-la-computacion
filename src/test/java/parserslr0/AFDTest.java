package parserslr0;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import lectorDeArchivos.LectorDeArchivos;

public class AFDTest {

	String ruta2 = "parsers/GLCaParserLR(0)2.txt";
	List<String> archivo2 = LectorDeArchivos.leerArchivo(ruta2);
	InterpreteGLC glc2 ;
	
	String ruta3 = "parsers/GLCaParserLR(0)1.txt";
	List<String> archivo3 = LectorDeArchivos.leerArchivo(ruta3);
	InterpreteGLC glc3 ;
	String ruta4 = "parsers/transicionEpsilon.txt";
	List<String> archivo4 = LectorDeArchivos.leerArchivo(ruta4);
	InterpreteGLC glc4 ;
	AnalizadorLR0 analizador2;
	AnalizadorLR0 analizador3;
	AnalizadorLR0 analizador4;
	
	AFD afd2;
	AFD afd3;
	AFD afd4;
	
	@Before
	public void init() {
		glc2 = new InterpreteGLC(archivo2);
		glc3 = new InterpreteGLC(archivo3);
		glc4 = new InterpreteGLC(archivo4);
		analizador2 = new AnalizadorLR0(glc2);
		analizador3 = new AnalizadorLR0(glc3);
		analizador4 = new AnalizadorLR0(glc4);
		afd2 = new AFD(analizador2);
		afd3 = new AFD(analizador3);
		afd4 = new AFD(analizador4);
	}
	
	
	@Test
	public void getEstadosTest() {
		assertEquals(6,this.afd3.getEstados().size());
	}
	
	@Test
	public void getTransicionesTest() {
		assertEquals(7,this.afd3.getTransiciones().size());
	}

}
