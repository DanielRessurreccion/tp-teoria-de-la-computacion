package normalizadorFNC;

import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.commons.math3.util.Pair;
import org.junit.Before;
import org.junit.Test;
import interpreteDeProducciones.Interprete;
import lectorDeArchivos.LectorDeArchivos;

public class NormalizadorFNCtest {

	String ruta = "Producciones ejemplo2.txt";
	ArrayList<String> resultado = LectorDeArchivos.leerArchivo(ruta);		
	Interprete interp = new Interprete();
	HashMap<String,ArrayList<String>> prod = interp.interpretarProducciones(resultado);		
	NormalizadorFNC n = new NormalizadorFNC(prod); 	
	HashMap<String, ArrayList<String>> prodsEsperadas = new HashMap<String, ArrayList<String>>(); 
	ArrayList<String> arr1 = new ArrayList<String>(); 
	
	@Before
	public void iniciar() {
		arr1.add("@");
		arr1.add("bSb");
		arr1.add("aSa");
		prodsEsperadas.put("S", arr1);
	}	
		
	@Test
	public void getProduccionesSinNulleablesTest() {
		assertEquals(prodsEsperadas, n.getResultadoSinNulleables());
	}
		
	@Test
	public void getProduccionesSinUnitariasTest() {
		assertEquals(prodsEsperadas, n.getResultadoSinUnitarias());
	}
	
	@Test
	public void getProduccionesConSoloGeneradoresTest() {
		assertEquals(prodsEsperadas, n.getResultadoSoloGeneradores());
	}
	
	@Test
	public void getProduccionesConSoloAlcanzablesTest() {
		assertEquals(prodsEsperadas, n.getResultadoSoloAlcanzables());
	}
	
	@Test
	public void getTuplasFNC() {
		ArrayList<Pair<String,String>> tuplasEsperadas = new	ArrayList<Pair<String,String>>();
		tuplasEsperadas.add(new Pair<String,String> ("S","@") );
		tuplasEsperadas.add(new Pair<String,String> ("S", "A{0}C{0}") );		
		tuplasEsperadas.add(new Pair<String,String> ("C{0}", "SA{0}") );		
		tuplasEsperadas.add(new Pair<String,String> ("S", "B{0}D{0}") );		
		tuplasEsperadas.add(new Pair<String,String> ("D{0}", "SB{0}") );
		tuplasEsperadas.add(new Pair<String,String> ("A{0}","b") );
		tuplasEsperadas.add(new Pair<String,String> ("B{0}","a") );
		assertEquals(tuplasEsperadas, n.getTuplasFNC());	
	}
	
	
}
