package lectorDeArchivos;

import static org.junit.Assert.*;
import java.util.ArrayList;
import org.junit.Test;

public class lectorDeArchivosTest {

	@Test
	public void leerArchivoVacioTest() {
		ArrayList<String> arrayArchivo = LectorDeArchivos.leerArchivo("ProduccionesVacioTest.txt");  
		ArrayList<String> arrayVacio = new ArrayList<String>();
		assertEquals(arrayArchivo, arrayVacio);
	}
	
	@Test
	public void leerArchivoConProduccionesTest() {
		ArrayList<String> arrayArchivo = LectorDeArchivos.leerArchivo("ProduccionesTestExitoso1.txt");  
		ArrayList<String> arrayConProducciones = new ArrayList<String>();
		arrayConProducciones.add("S --> bAb | aBa");
		arrayConProducciones.add("A --> a");
		arrayConProducciones.add("B --> b");		
		assertEquals(arrayArchivo, arrayConProducciones);
	}
	
	@Test 
	public void leerArchivoRutaErroneaTest() {
		try {
			LectorDeArchivos.leerArchivo("RutaErronea.txt");
		}
		catch(Exception e) {
			assertTrue(true);
		}
	}
}